from gym import Env, logger, spaces, utils
import gym
import numpy as np 

class MultiDiscreteActionSpaceWrapper(gym.Wrapper):
    def __init__(self, env):
        super().__init__(env)
        self.action_space = spaces.MultiDiscrete([env.unwrapped.num_rows,env.unwrapped.num_cols, env.unwrapped.number_of_rotations])


    def step(self, action):
        if action[1]==5:
            print(action)
        obs, reward, terminated, truncated, info = self.env.step(action)
       
        return obs, reward, terminated, truncated, info