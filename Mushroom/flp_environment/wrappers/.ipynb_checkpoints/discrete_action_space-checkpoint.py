from gym import Env, logger, spaces, utils
import gym
import numpy as np 

class DiscreteActionSpaceWrapper(gym.Wrapper):
    def __init__(self, env):
        super().__init__(env)
        self.action_space = spaces.Discrete(env.unwrapped.nS*env.unwrapped.number_of_rotations)


    def step(self, action):
       
        num_cols =self.env.unwrapped.num_cols
        num_rotations= self.env.unwrapped.number_of_rotations
        rotation = int(action % num_rotations)
        position = int(action / num_rotations)

        row = int(position / num_cols)
        col = int(position % num_cols)

            
        action= [row,col,rotation]
       
        obs, reward, terminated, info = self.env.step(action)
       
        return obs, reward, terminated, info