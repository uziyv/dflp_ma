import gym

class FullEpisodeRunner(gym.Wrapper):
    def __init__(self, env,max_episode_steps):
        super().__init__(env)
        self.env=env
        self.step_count= 0
        self.done=False
        self.last_state=None 
        self.max_episode_steps=max_episode_steps
       


    def step(self, action):
        self.step_count+=1
        if(self.done):  
            return self.last_state,-160,self.step_count==self.max_episode_steps,{}
        self.last_state, reward,done,info=self.env.step(action)
        
        if(reward<0):
            self.done=True
            return self.last_state,-160,self.step_count==self.max_episode_steps,{}
        return self.last_state, reward,self.step_count==self.max_episode_steps,info
        
       
    def reset(self,seed=None,options=None):
        self.step_count= 0
        self.done=False
        self.last_state=None 
        return self.env.reset()
