import logging
from gym.envs.registration import register

logger = logging.getLogger(__name__)

register(
    id='flp-v2',
    entry_point='flp_environment.envs:environment_v_2',
   
)

register(
    id='flp-v1',
    entry_point='flp_environment.envs:environment',
    kwargs={} 
   
)

register(  id='flp_discrete-v1',
          entry_point='flp_environment.envs:FLP_Environment_discrete') 

register(id = "flp_cont-v1", 
          entry_point='flp_environment.envs:FLP_Environment_Continous')

