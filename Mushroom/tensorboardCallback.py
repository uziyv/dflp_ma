import datetime
import pathlib
from mushroom_rl.utils.callbacks.collect_dataset import CollectDataset
from mushroom_rl.utils.dataset import compute_episodes_length, compute_J
import numpy as np
from mushroom_rl.utils.plots.databuffer import DataBuffer
from torch.utils.tensorboard import SummaryWriter
class TensorboardCallBack(CollectDataset):
    def __init__(self,path,hyperParameters):
        
        super().__init__()
        print( )
        self.writer = SummaryWriter(path + "/tb/"  )
        print(
            "\nTraining...\nRun tensorboard --logdir ./results and go to http://localhost:6006/ to see in real time the training performance.\n"
        )
        print("\n hyps: \n", hyperParameters)
        # Save hyperparameters to TensorBoard
        hp_table = [
            f"| {key} | {value} |" for key, value in hyperParameters.items()
        ]
        self.writer.add_text(
            "Hyperparameters",
            "| Parameter | Value |\n|-------|-------|\n" + "\n".join(hp_table),
        )
        # Save model representation
        '''self.writer.add_text(
            "Model summary",
            self.summary,
        )'''
        self.training_reward_buffer = DataBuffer("Episode_reward",100)

        self.episodic_len_buffer_training = DataBuffer("Episode_len",100)
        self.counter = 0




    
    def __call__(self, dataset):
        super().__call__(dataset)
        if(self.counter%100 !=0):
            self.counter+=1
            return
      
    

        lengths_of_episodes = compute_episodes_length(self._data_list)


        start_index = 0 

      
       
        for length_of_episode in lengths_of_episodes:
            sub_dataset = self._data_list[start_index:start_index+length_of_episode]
            #if(len(sub_dataset)>1):
            #    print(sub_dataset)
            #    print("length: ", length_of_episode)
            #    print("length subset: ",len(sub_dataset))
            episodic_reward = compute_J(sub_dataset)
           
            
            self.training_reward_buffer.update([episodic_reward[0]])
            self.episodic_len_buffer_training.update([length_of_episode])
            start_index += length_of_episode
    
    
    
        if( np.mean(lengths_of_episodes) == np.mean(lengths_of_episodes)):
            self.writer.add_scalar("rollout/length",np.mean(self.episodic_len_buffer_training.get()),self.counter)
            self.writer.add_scalar("rollout/reward",np.mean(self.training_reward_buffer.get()),self.counter)
            self.counter+=1
           
        self._data_list = self._data_list[start_index:]
    
    def write_eval(self,time,eval_dataset):
        lengths_of_episodes = compute_episodes_length(eval_dataset)
        episodic_reward = compute_J(eval_dataset)
        self.writer.add_scalar("eval/length",np.mean(lengths_of_episodes),time)
        self.writer.add_scalar("eval/reward",np.mean(episodic_reward),time)

    def write_end_result(self, results):
        results_table = [
            f"| {result} | " for result in results
        ]
        self.writer.add_text(
            "Results",
            "| Result |\n|-------|\n" + "\n".join(results_table),
        )

        self.writer.add_text("mean Results", str(np.mean(results)))
  
    




