from copy import deepcopy
import datetime
import os
import pathlib
import numpy as np
import gym
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from mushroom_rl.approximators.parametric import LinearApproximator
from mushroom_rl.approximators.regressor import Regressor
from mushroom_rl.policy import StateStdGaussianPolicy,ClippedGaussianPolicy,Boltzmann,GaussianPolicy,Mellowmax, OrnsteinUhlenbeckPolicy,BoltzmannTorchPolicy,StateLogStdGaussianPolicy,GaussianTorchPolicy
from mushroom_rl.algorithms.value import DQN,Rainbow, DoubleDQN
from mushroom_rl.core import Core, Logger
from mushroom_rl.environments import *
from mushroom_rl.policy import EpsGreedy
from mushroom_rl.approximators.parametric.torch_approximator import *
from mushroom_rl.utils.dataset import compute_J
from mushroom_rl.utils.parameters import Parameter, LinearParameter
from mushroom_rl.policy import BoltzmannTorchPolicy
from mushroom_rl.algorithms.actor_critic import A2C,SAC,PPO,TRPO,DDPG
from mushroom_rl.utils.dataset import compute_J, parse_dataset
from gym.envs.registration import register
from mushroom_rl.utils.replay_memory import PrioritizedReplayMemory
from mushroom_rl.algorithms.policy_search import REINFORCE
from mushroom_rl.utils.optimizers import AdaptiveOptimizer
#from mushroom_rl.utils.callbacks.plot_dataset import PlotDataset
from tensorboardCallback import TensorboardCallBack
from flp_environment.wrappers.continous_action_space import ContinousActionSpaceWrapper
from flp_environment.wrappers.discrete_action_space import DiscreteActionSpaceWrapper
from flp_environment.wrappers.full_episode_runner import FullEpisodeRunner
import yaml
import flp_environment
LEFT = 0
DOWN = 1
RIGHT = 2
UP = 3
NOTHING = 4

ROTATE90 = 0
ROTATE180 = 1
ROTATE270 = 2
ROTATE360 = 3

EMPTY = 0
OBSTACLE = -1
START_OF_FLOW = -2
END_OF_FLOW = -3

'''
desc = [[[EMPTY, EMPTY, EMPTY, START_OF_FLOW, EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, START_OF_FLOW, EMPTY, OBSTACLE, EMPTY, OBSTACLE]],
        [[OBSTACLE, OBSTACLE, EMPTY, EMPTY, EMPTY, EMPTY, OBSTACLE],
        [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, START_OF_FLOW],
        [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, OBSTACLE, EMPTY],
        [END_OF_FLOW, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, OBSTACLE]],

        [[START_OF_FLOW, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, END_OF_FLOW]]
        ]
# scenario
desc = [[[OBSTACLE, OBSTACLE, START_OF_FLOW, EMPTY, EMPTY],
         [OBSTACLE, OBSTACLE, EMPTY, EMPTY, EMPTY],
         [EMPTY, EMPTY, EMPTY, OBSTACLE, OBSTACLE],
         [EMPTY, EMPTY, END_OF_FLOW, EMPTY, EMPTY]
         ], [[OBSTACLE, OBSTACLE, START_OF_FLOW, EMPTY, EMPTY],
             [OBSTACLE, OBSTACLE, EMPTY, EMPTY, EMPTY],
             [EMPTY, EMPTY, EMPTY, OBSTACLE, OBSTACLE],
             [EMPTY, EMPTY, END_OF_FLOW, EMPTY, EMPTY]]]
units = [np.asarray([[1, 2], [EMPTY, 2]], dtype=np.dtype(int)), np.asarray(
    [[4, 5], [5, 5]], dtype=np.dtype(int))]

# scenario
units = [[[EMPTY, 2], [2, 1]],
         [[EMPTY, 5], [5, 4]]]

initial_positions = {0: [[0, 0]], 1: [[2, 4]]}

fix_cost = 1

material_flow = [[[0, 1]], [[1, 0]]]
# scenario
material_flow = [[[1, 0]], [[0, 1]]]


##
# material_flow = [[[1,0]],[[0,1]]]
interarrival_time = [[10], [100]]
period = 8

register(  id='flp_discrete-v1',
          entry_point='flp_environment.envs:FLP_Environment_discrete') 
#env= gym.make('flp_discrete-v1', desc= desc, units = units)
register(id = "flp_cont-v1", 
          entry_point='flp_environment.envs:FLP_Environment_Continous')

'''

class Network(nn.Module):
    def __init__(self, input_shape, output_shape, n_features, **kwargs):
        super().__init__()

        n_input = input_shape[-1]
        n_output = output_shape[0]

        self._h1 = nn.Linear(n_input, n_features)
        self._h2 = nn.Linear(n_features, n_features)
        self._h3 = nn.Linear(n_features, n_output)

        nn.init.xavier_uniform_(self._h1.weight,
                                gain=nn.init.calculate_gain('relu'))
        nn.init.xavier_uniform_(self._h2.weight,
                                gain=nn.init.calculate_gain('relu'))
        nn.init.xavier_uniform_(self._h3.weight,
                                gain=nn.init.calculate_gain('linear'))

    def forward(self, state, action=None):
        features1 = F.relu(self._h1(torch.squeeze(state, 1).float()))
        features2 = F.relu(self._h2(features1))
        q = self._h3(features2)

        if action is None:
            return q
        else:
            action = action.long()
            q_acted = torch.squeeze(q.gather(1, action))

            return q_acted


class FeatureNetwork(nn.Module):
    
    def __init__(self, input_shape, output_shape,n_features, **kwargs):
        super().__init__()

        n_input_channels = input_shape[0]
        self.cnn = nn.Sequential(
            nn.Conv2d(in_channels=n_input_channels,out_channels=32,kernel_size=3),   
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2),
            nn.Conv2d(in_channels=32,out_channels=64,kernel_size=3),   
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2),
            nn.Flatten()
        )

        n_flatten = 256
        self.linear = nn.Sequential(nn.Linear(n_flatten, n_features), nn.ReLU())

    def forward(self, state, action=None):
        state = state.float()
      
        encoded_state= self.linear(self.cnn(state))
        
        return encoded_state


class CriticNetwork(nn.Module):
    def __init__(self, input_shape, output_shape, n_features, **kwargs):
        super().__init__()

        n_input = input_shape[-1]
        n_output = output_shape[0]
 

        self._h1 = nn.Linear(n_input, n_features)
        self._h2 = nn.Linear(n_features, n_features)
        self._h3 = nn.Linear(n_features, n_output)

        nn.init.xavier_uniform_(self._h1.weight,
                                gain=nn.init.calculate_gain('relu'))
        nn.init.xavier_uniform_(self._h2.weight,
                                gain=nn.init.calculate_gain('relu'))
        nn.init.xavier_uniform_(self._h3.weight,
                                gain=nn.init.calculate_gain('linear'))

    def forward(self, state, action):
     
   
        state_action = torch.cat((state.float(), action.float()), dim=1)

        features1 = F.relu(self._h1(state_action))
        features2 = F.relu(self._h2(features1))
        q = self._h3(features2)
       
        return torch.squeeze(q)


class ActorNetwork(nn.Module):
    def __init__(self, input_shape, output_shape, n_features, **kwargs):
        super(ActorNetwork, self).__init__()

        n_input = input_shape[-1]
        n_output = output_shape[0]
    
        self._h1 = nn.Linear(n_input, n_features)
        self._h2 = nn.Linear(n_features, n_features)
        self._h3 = nn.Linear(n_features, n_output)

        nn.init.xavier_uniform_(self._h1.weight,
                                gain=nn.init.calculate_gain('relu'))
        nn.init.xavier_uniform_(self._h2.weight,
                                gain=nn.init.calculate_gain('relu'))
        nn.init.xavier_uniform_(self._h3.weight,
                                gain=nn.init.calculate_gain('linear'))

    def forward(self, state):
        
        features1 = F.relu(self._h1(torch.squeeze(state, 1).float()))
        features2 = F.relu(self._h2(features1))
        a = self._h3(features2)

       
        return a



with open('config.yaml', 'r') as file:
    config = yaml.safe_load(file)
with open('env_config.yaml', 'r') as file:
    env_config = yaml.safe_load(file)
env_config=env_config["Use-case-2"]
agent_seed= config["general"]["agent_seed"]


def prepare_units(units):
    units_copy = deepcopy(units)
    for i in range(0,len(units_copy)):
        unit = units_copy[i]
        for j in range(0,len(unit)):
            for k in range(0,len(unit[0])):
                unit[j][k]= unit[j][k]+i*3 if unit[j][k] != 0 else 0

    
    return units_copy



def make_env():
    grids = env_config["grids"]
    units = prepare_units(env_config["units"])
    material_flow= env_config["material_flow"]
    fix_cost = env_config["fix_cost"]
    period= env_config["period"]
    interarrival_time= env_config["interarrival_time"]
    initial_positions =env_config["initial_positions"]
    env = Gym('flp_discrete-v1', horizon, gamma,desc= grids, units = units, material_flow=material_flow, fix_cost=fix_cost, interarrival_time=interarrival_time,period_length=period,initial_positions=initial_positions) #env = ObservationNormalizerWrapper(env)
    #env = NormalizeReward(env)
    #env = TimeLimit(env=env, max_episode_steps=len(units)*len(grids))
    return env



horizon = 1000
gamma = 0.99
gamma_eval = 1.
mdp = make_env()
mdp.env= DiscreteActionSpaceWrapper(mdp.env)
#mdp_cont = Gym('flp_cont-v1', horizon, gamma,desc= desc, units = units, material_flow=material_flow, fix_cost=fix_cost, initial_positions=initial_positions)
def dqn_experiment(n_epochs, n_steps, n_steps_test):
    np.random.seed()

    logger = Logger(DQN.__name__, results_dir=None)
    logger.strong_line()
    logger.info('Experiment Algorithm: ' + DQN.__name__)

    # MDP
    horizon = 1000
    gamma = 0.99
    gamma_eval = 1.
    #mdp = Gym('flp_discrete-v1', horizon, gamma,desc= desc, units = units)
    
    # Policy
    epsilon = LinearParameter(value=.4, threshold_value=.1, n=n_epochs*n_steps/10)
    epsilon_test = Parameter(value=0.)
    epsilon_random = Parameter(value=1.)
    pi = EpsGreedy(epsilon=epsilon)

    # Settings
    initial_replay_size = 500
    max_replay_size = 3000
    target_update_frequency = 100
    batch_size = 20
    n_features = 80
    train_frequency = 1

    # Approximator
    input_shape = mdp.info.observation_space.shape
    approximator_params = dict(network=Network,
                               optimizer={'class': optim.Adam,
                                          'params': {'lr': .0001}},
                               loss=F.smooth_l1_loss,
                               n_features=n_features,
                               input_shape=input_shape,
                               output_shape=mdp.info.action_space.size,
                               n_actions=mdp.info.action_space.n)

    # Agent
    agent = DQN(mdp.info, pi, TorchApproximator,
                approximator_params=approximator_params, batch_size=batch_size,
                initial_replay_size=initial_replay_size,
                max_replay_size=max_replay_size,
                target_update_frequency=target_update_frequency)

    #plotter = PlotDataset(mdp.info, obs_normalized=False)

    

    core = Core(agent, mdp)#,callback_step=plotter)

    core.learn(n_steps=initial_replay_size, n_steps_per_fit=initial_replay_size)

    # RUN
    pi.set_epsilon(epsilon_test)
    dataset = core.evaluate(n_steps=n_steps_test, render=False)
    J = compute_J(dataset, gamma_eval)
    logger.epoch_info(0, J=np.mean(J))

    for n in trange(n_epochs):
        pi.set_epsilon(epsilon)
        core.learn(n_steps=n_steps, n_steps_per_fit=train_frequency)
        pi.set_epsilon(epsilon_test)
        dataset = core.evaluate(n_steps=n_steps_test, render=False)
        J = compute_J(dataset, gamma_eval)
        logger.epoch_info(n+1, J=np.mean(J))
        

    logger.info('Press a button to visualize acrobot')

    core.evaluate(n_episodes=5, render=False)
    logger.info('Saving plotting and normalization data')
    os.makedirs("./logs/plot_and_norm", exist_ok=True)
    #prepro.save("./logs/plot_and_norm/preprocessor.msh")
    #plotter.save_state("./logs/plot_and_norm/plotting_state")
    return agent

def rainbow_experiment(max_steps_training, evaluation_frequency_training, n_episodes_test,params,path):

    seed=config["general"]["agent_seed"]
    #np.random.seed(seed)
    #torch.manual_seed(seed)
    logger = Logger(DQN.__name__, results_dir=None) 
    logger.strong_line()
    logger.info('Experiment Algorithm: ' + Rainbow.__name__)


 

    learning_rate = float(config["rainbow"]["learning_rate"])
    initial_replay_size= config["rainbow"]["initial_replay_size"]
    max_replay_size= config["rainbow"] ["max_replay_size"]
    train_frequency= config["rainbow"] ["train_frequency"]
    target_update_frequency= config["rainbow"] ["target_update_frequency"]
    test_samples= config["rainbow"] ["test_samples"]
    evaluation_frequency= config["rainbow"] ["evaluation_frequency"]
    max_steps= config["rainbow"] ["max_steps"]
    batch_size= config["rainbow"] ["batch_size"]
    n_features= config["rainbow"] ["n_features"]
    beta_initial= config["rainbow"] ["beta_initial"]
    beta_final= config["rainbow"] ["beta_final"]
    beta_steps= config["rainbow"] ["beta_steps"]
    n_atoms= config["rainbow"] ["n_atoms"]
    alpha= config["rainbow"] ["alpha"]
    sigma= config["rainbow"] ["sigma"]
    n_steps_return= config["rainbow"] ["n_steps_return"]

    hyperParameters={
        "learning_rate": learning_rate,
        "initial_replay_size": initial_replay_size,
        "max_replay_size":max_replay_size,
        "train_frequency":train_frequency,
        "target_update_frequency":target_update_frequency,
        "batch_size":batch_size,
        "n_features":n_features,
        "beta_initial": beta_initial,
        "beta_final":beta_final,
        "beta_steps":beta_steps,
        "n_atoms":n_atoms,
        "alpha":alpha,
        "sigma":sigma,
        "n_steps_return":n_steps_return


    }
   
    beta = LinearParameter(beta_initial, threshold_value=beta_final, n=beta_steps)
   #,           'eps': 1e-4
                                          
    input_shape = mdp.info.observation_space.shape
    approximator_params = dict(network = FeatureNetwork,
                               optimizer={'class': optim.Adam,
                                          'params': {'lr': learning_rate}},
                               
                               n_features=n_features,
                               
                               output_shape=mdp.info.action_space.size,
                                input_shape=input_shape,
                               n_actions=mdp.info.action_space.n,
                                #use_cuda=True
                              
                               )


    gamma_eval = 1.
    epsilon_training = LinearParameter(0.4, threshold_value =0.4,n=max_steps_training*0.1)
    epsilon_test = Parameter(value=0.)
    pi = EpsGreedy(epsilon=epsilon_test)

  
    agent = Rainbow(mdp.info,pi,approximator_params=approximator_params,n_atoms=n_atoms,alpha_coeff=alpha,n_steps_return=n_steps_return, beta =beta, v_max=16,v_min =-1,sigma_coeff=sigma,
                     initial_replay_size = initial_replay_size, max_replay_size = max_replay_size, batch_size= batch_size, target_update_frequency=target_update_frequency)
    #plotter = PlotDataset(mdp.info, obs_normalized=False,update_freq=100)
   
    callback = TensorboardCallBack(path,hyperParameters)
    core = Core(agent, mdp,callback_step=callback)
    

    core.learn(n_steps=initial_replay_size, n_steps_per_fit=train_frequency)
    # RUN
    pi.set_epsilon(epsilon_test)
    dataset = core.evaluate(n_episodes=n_episodes_test, render=False)
    callback.write_eval(0, dataset)
    
    for n_epoch in range(1, max_steps_training // evaluation_frequency_training + 1):
        #pi.set_epsilon(epsilon_training)
        core.learn(n_steps=evaluation_frequency_training, n_steps_per_fit=train_frequency)
        pi.set_epsilon(epsilon_test)
        dataset = core.evaluate(n_episodes=n_episodes_test, render=False)
        callback.write_eval(n_epoch*evaluation_frequency_training, dataset)
        

    pi.set_epsilon(epsilon=epsilon_test)
    dataset = core.evaluate(n_episodes=n_episodes_test, render=False)
    J = compute_J(dataset, gamma_eval)
    callback.write_end_result(J)
    logger.info('Saving plotting and normalization data')
    agent.save(path+"/models/bestModel",full_save=True)
    
    os.makedirs("./logs/plot_and_norm", exist_ok=True)
 
    return agent,np.mean(J)
    '''seed=config["general"]["agent_seed"]
    np.random.seed(seed)
    torch.manual_seed(seed)
    logger = Logger(DQN.__name__, results_dir=None) 
    logger.strong_line()
    logger.info('Experiment Algorithm: ' + Rainbow.__name__)'''


 

    '''learning_rate = float(config["rainbow"]["learning_rate"])
    initial_replay_size= config["rainbow"]["initial_replay_size"]
    max_replay_size= config["rainbow"] ["max_replay_size"]
    train_frequency= config["rainbow"] ["train_frequency"]
    target_update_frequency= config["rainbow"] ["target_update_frequency"]
    test_samples= config["rainbow"] ["test_samples"]
    evaluation_frequency= config["rainbow"] ["evaluation_frequency"]
    max_steps= config["rainbow"] ["max_steps"]
    batch_size= config["rainbow"] ["batch_size"]
    n_features= config["rainbow"] ["n_features"]
    beta_initial= config["rainbow"] ["beta_initial"]
    beta_final= config["rainbow"] ["beta_final"]
    beta_steps= config["rainbow"] ["beta_steps"]
    n_atoms= config["rainbow"] ["n_atoms"]
    alpha= config["rainbow"] ["alpha"]
    sigma= config["rainbow"] ["sigma"]
    n_steps_return= config["rainbow"] ["n_steps_return"]'''
    '''beta_initial = params["beta_initial"]
    beta_final = params["beta_final"]
    train_frequency = params["train_frequency"]
    beta = LinearParameter(beta_initial, threshold_value=beta_final, n=max_steps // train_frequency)
   #,           'eps': 1e-4
                                          
    input_shape = mdp.info.observation_space.shape
    approximator_params = dict(network = FeatureNetwork,
                               optimizer={'class': optim.Adam,
                                          'params': {'lr': learning_rate}},
                               
                               n_features=n_features,
                               input_shape=input_shape,
                               output_shape=mdp.info.action_space.size,
                               n_actions=mdp.info.action_space.n,

                              
                               )


    gamma_eval = 1.

    epsilon_test = Parameter(value=0.)
    pi = EpsGreedy(epsilon=epsilon_test)
    epsilon = LinearParameter(value=args.initial_exploration_rate,
                                  threshold_value=args.final_exploration_rate,
                                  n=args.final_exploration_frame)
    epsilon_test = Parameter(value=args.test_exploration_rate)
    epsilon_random = Parameter(value=1)
    pi = EpsGreedy(epsilon=epsilon_random)

  
    agent = Rainbow(mdp.info,pi,**params)
    plotter = PlotDataset(mdp.info, obs_normalized=False,update_freq=100)
    core = Core(agent, mdp,callback_step=plotter)
    
    core = Core(agent, mdp,callback_step=plotter)

        # RUN

        # Fill replay memory with random dataset
    print_epoch(0, logger)
    core.learn(n_steps=initial_replay_size, n_steps_per_fit=initial_replay_size)

    if args.save:
        agent.save(folder_name + '/agent_0.msh')

        # Evaluate initial policy
    pi.set_epsilon(epsilon_test)
    dataset = core.evaluate(n_episodes=test_episodes, render=args.render,
                                quiet=args.quiet)
    scores.append(get_stats(dataset, logger))

    np.save(folder_name + '/scores.npy', scores)
    for n_epoch in range(1, max_steps // evaluation_frequency + 1):
        print_epoch(n_epoch, logger)
        logger.info('- Learning:')
        # learning step
        pi.set_epsilon(epsilon)
        core.learn(n_steps=evaluation_frequency, n_steps_per_fit=train_frequency)

        if args.save:
            agent.save(folder_name + '/agent_' + str(n_epoch) + '.msh')

        logger.info('- Evaluation:')
            # evaluation step
        pi.set_epsilon(epsilon_test)
        dataset = core.evaluate(n_episodes=test_episodes, render=args.render,
                                    quiet=args.quiet)
        scores.append(get_stats(dataset, logger))

        np.save(folder_name + '/scores.npy', scores)

    return agent, scores'''
    '''core.learn(n_steps=initial_replay_size, n_steps_per_fit=train_frequency)
    # RUN
    pi.set_epsilon(epsilon_test)
    dataset = core.evaluate(n_steps=n_steps_test, render=False)
    J = compute_J(dataset, gamma_eval)
    logger.epoch_info(0, J=np.mean(J))

    for n in trange(n_epochs):
      
        core.learn(n_steps=n_steps, n_steps_per_fit=train_frequency)

        dataset = core.evaluate(n_steps=n_steps_test, render=False)
        J = compute_J(dataset, gamma_eval)
        logger.epoch_info(n+1, J=np.mean(J))
        

    core.evaluate(n_episodes=5, render=False)
    pi.set_epsilon(epsilon=epsilon_test)
    core.evaluate(n_episodes=5, render=False)
    logger.info('Saving plotting and normalization data')
    agent.save("./models/bestModel",full_save=True)
    
    os.makedirs("./logs/plot_and_norm", exist_ok=True)
    #prepro.save("./logs/plot_and_norm/preprocessor.msh")
    plotter.save_state("./logs/plot_and_norm/plotting_state")
    return agent'''


def ddqn_experiment(n_epochs, n_steps, n_steps_test):
    np.random.seed()
    logger = Logger(DQN.__name__, results_dir=None) 
    logger.strong_line()
    logger.info('Experiment Algorithm: ' + DQN.__name__)

  # Settings
    initial_replay_size = 500
    max_replay_size = 3000
    train_frequency = 1
    target_update_frequency = 100
    test_samples = 20
    evaluation_frequency = 50
    max_steps = 4
    batch_size = 20
    n_features=80
    epsilon_random = Parameter(value=1.)
    pi = EpsGreedy(epsilon=epsilon_random)
    beta = LinearParameter(.4, threshold_value=1, n=max_steps // train_frequency)

    input_shape = mdp.info.observation_space.shape
    approximator_params = dict(network = Network,
                               optimizer={'class': optim.Adam,
                                          'params': {'lr': .005}},
                             loss=F.smooth_l1_loss,
                               n_features=n_features,
                               input_shape=input_shape,
                               output_shape=mdp.info.action_space.size,
                               n_actions=mdp.info.action_space.n,
                              
                               )


    # MDP
    horizon = 1000
    gamma = 0.99
    gamma_eval = 1.
    #mdp = Gym('flp_discrete-v1', horizon, gamma,desc= desc, units = units)
    
    # Policy
    epsilon = LinearParameter(value=.4, threshold_value=.1, n=n_epochs*n_steps*0.1)
    epsilon_test = Parameter(value=0.)
    epsilon_random = Parameter(value=1.)
    pi = EpsGreedy(epsilon=epsilon)

  

    approximator = TorchApproximator
    agent = DoubleDQN(mdp.info,pi,approximator_params=approximator_params ,approximator=approximator,
                     initial_replay_size = initial_replay_size, max_replay_size = max_replay_size, batch_size= batch_size, target_update_frequency=target_update_frequency)
    core = Core(agent, mdp)

    core.learn(n_steps=initial_replay_size, n_steps_per_fit=initial_replay_size)

    # RUN
    pi.set_epsilon(epsilon_test)
    dataset = core.evaluate(n_steps=n_steps_test, render=False)
    J = compute_J(dataset, gamma_eval)
    logger.epoch_info(0, J=np.mean(J))

    for n in trange(n_epochs):
        pi.set_epsilon(epsilon)
        core.learn(n_steps=n_steps, n_steps_per_fit=train_frequency)
        pi.set_epsilon(epsilon_test)
        dataset = core.evaluate(n_steps=n_steps_test, render=False)
        J = compute_J(dataset, gamma_eval)
        logger.epoch_info(n+1, J=np.mean(J))
        

    logger.info('Press a button to visualize acrobot')

    core.evaluate(n_episodes=5, render=False)
    return agent



def a2c_experiment(n_epochs, n_steps, n_steps_per_fit, n_step_test):
    np.random.seed()

    logger = Logger(A2C.__name__, results_dir=None)
    logger.strong_line()
    logger.info('Experiment Algorithm: ' + A2C.__name__)

    # MDP
    horizon = 1000
    gamma = 0.99
    gamma_eval = 1.
   

    # Policy
    policy_params = dict(
        n_features=32,
        use_cuda=False
    )

    beta = Parameter(1e0)
 
    pi = BoltzmannTorchPolicy(Network,
                              mdp.info.observation_space.shape,
                              (mdp.info.action_space.n,),
                              beta=beta,
                              **policy_params)

    # Agent
    critic_params = dict(network=Network,
                         optimizer={'class': optim.RMSprop,
                                    'params': {'lr': 1e-4,
                                               'eps': 1e-4}},
                         loss=F.mse_loss,
                         n_features=32,
                         batch_size=64,
                         input_shape=mdp.info.observation_space.shape,
                         output_shape=(1,))

    alg_params = dict(actor_optimizer={'class': optim.RMSprop,
                                       'params': {'lr': 5e-4
                                                 }},
                      critic_params=critic_params,
                      ent_coeff=0.1
                      )

    agent = A2C(mdp.info, pi, **alg_params)

    # Algorithm
    callback = TensorboardCallBack()
    core = Core(agent, mdp,callback_step=callback)
   
    core.learn(n_steps=n_steps, n_steps_per_fit=n_steps_per_fit)

    # RUN
    dataset = core.evaluate(n_steps=n_step_test, render=False)
    J = compute_J(dataset, gamma_eval)
    logger.epoch_info(0, J=np.mean(J))

    for n in trange(n_epochs):
        core.learn(n_steps=n_steps, n_steps_per_fit=n_steps_per_fit)
        dataset = core.evaluate(n_steps=n_step_test, render=False)
        J = compute_J(dataset, gamma_eval)
        logger.epoch_info(n+1, J=np.mean(J))

    logger.info('Press a button to visualize acrobot')
    input()
    core.evaluate(n_episodes=5, render=True)
    return agent

def sac_experiment( n_epochs, n_steps, n_steps_test):
    np.random.seed()

    logger = Logger( results_dir=None)
   
    # MDP
    horizon = 200
    gamma = 0.99
 

    # Settings
    initial_replay_size = 500
    max_replay_size = 5000
    batch_size = 32
    n_features = 32
    warmup_transitions = 100
    tau = 0.0001
    lr_alpha = 3e-4

    use_cuda = torch.cuda.is_available()

    # Approximator
    actor_input_shape = mdp_cont.info.observation_space.shape
    actor_mu_params = dict(network=ActorNetwork,
                           n_features=n_features,
                           input_shape=actor_input_shape,
                           output_shape=mdp_cont.info.action_space.shape,
                           use_cuda=use_cuda)
    actor_sigma_params = dict(network=ActorNetwork,
                              n_features=n_features,
                              input_shape=actor_input_shape,
                              output_shape=mdp_cont.info.action_space.shape,
                              use_cuda=use_cuda)

    actor_optimizer = {'class': optim.Adam,
                       'params': {'lr': 3e-4}}

    critic_input_shape = (actor_input_shape[0] + mdp_cont.info.action_space.shape[0],)
    critic_params = dict(network=CriticNetwork,
                         optimizer={'class': optim.Adam,
                                    'params': {'lr': 3e-4}},
                         loss=F.mse_loss,
                         n_features=n_features,
                         input_shape=critic_input_shape,
                         output_shape=(1,),
                         use_cuda=use_cuda)

    # Agent
    agent = SAC(mdp_cont.info, actor_mu_params, actor_sigma_params,
                actor_optimizer, critic_params, batch_size, initial_replay_size,
                max_replay_size, warmup_transitions, tau, lr_alpha,
                critic_fit_params=None)

    # Algorithm
    core = Core(agent, mdp_cont)

    # RUN
    dataset = core.evaluate(n_steps=n_steps_test, render=False)
    s, *_ = parse_dataset(dataset)

    J = np.mean(compute_J(dataset, mdp_cont.info.gamma))
    R = np.mean(compute_J(dataset))
    E = agent.policy.entropy(s)

    logger.epoch_info(0, J=J, R=R, entropy=E)

    core.learn(n_steps=initial_replay_size, n_steps_per_fit=initial_replay_size)

    for n in trange(n_epochs, leave=False):
        core.learn(n_steps=n_steps, n_steps_per_fit=1)
        dataset = core.evaluate(n_steps=n_steps_test, render=False)
        s, *_ = parse_dataset(dataset)

        J = np.mean(compute_J(dataset, mdp_cont.info.gamma))
        R = np.mean(compute_J(dataset))
        E = agent.policy.entropy(s)

        logger.epoch_info(n+1, J=J, R=R, entropy=E)

    logger.info('Press a button to visualize pendulum')
    input()
    core.evaluate(n_episodes=5, render=True)
    return agent

def ppo_experiment( n_epochs, n_steps, n_steps_test, n_steps_per_fit):
    np.random.seed(1)
    torch.manual_seed(1)
    logger = Logger( results_dir=None)
    # prepare logging


    # create the policy
    dim_env_state = mdp.info.observation_space.shape[0]
    dim_action = mdp.info.action_space.shape[0]
    policy_params = dict(
        n_features=32,
        use_cuda=False
    )
    beta = Parameter(1e0)
    pi = BoltzmannTorchPolicy(Network,
                              mdp.info.observation_space.shape,
                              (mdp.info.action_space.n,),
                              beta=beta,
                              **policy_params)


    # setup critic
    critic_params = dict(network=Network,
                         optimizer={'class': optim.RMSprop,
                                    'params': {'lr': 1e-4,
                                               'eps': 1e-5}},
                         loss=F.mse_loss,
                         n_features=32,
                         batch_size=64,
                         input_shape=mdp.info.observation_space.shape,
                         output_shape=(1,))

    alg_params = dict(actor_optimizer={'class':  optim.Adam,
                                       'params': {'lr': 1e-3,
                                                  'weight_decay': 0.0}},
        
                      )

    # Create the agent
    agent = PPO(mdp_info=mdp.info,policy =pi,n_epochs_policy=1,batch_size=32,eps_ppo=0.2,lam=0.1 ,critic_params=critic_params, **alg_params)

    # Create Core
    core = Core(agent, mdp)

    # Evaluation
    dataset = core.evaluate(n_steps=n_steps_test, render=False)
    J = compute_J(dataset, gamma_eval)
    logger.epoch_info(0, J=np.mean(J))

    for n in trange(n_epochs):
        core.learn(n_steps=n_steps, n_steps_per_fit=n_steps_per_fit)
        dataset = core.evaluate(n_steps=n_steps_test, render=False)
        J = compute_J(dataset, gamma_eval)
        logger.epoch_info(n+1, J=np.mean(J))

    logger.info('Press a button to visualize acrobot')
    input()
    core.evaluate(n_episodes=5, render=True)
    return agent

def vpg_experiment( n_epochs, n_steps, n_steps_test):
    np.random.seed()

    logger = Logger(REINFORCE.__name__, results_dir=None)
    logger.strong_line()
    logger.info('Experiment Algorithm: ' + REINFORCE.__name__)


    n_action=Parameter(112,min_value=0,max_value=111,size=(3,))
    approximator = Regressor(LinearApproximator,
                             input_shape=mdp.info.observation_space.shape,
                             output_shape=mdp.info.action_space.shape)

    sigma = Regressor(LinearApproximator,
                      input_shape=mdp.info.observation_space.shape,
                      output_shape=mdp.info.action_space.shape)

    sigma_weights = 0.1*np.ones(sigma.weights_size)
    sigma.set_weights(sigma_weights)
    approximator_weights = -2* np.ones(approximator.weights_size)
    #approximator.set_weights(approximator_weights)
    policy = StateStdGaussianPolicy(approximator, sigma)
    

   
    # Agent
    optimizer = AdaptiveOptimizer(eps=1e-4)
    algorithm_params = dict(optimizer=optimizer)
    agent = REINFORCE(mdp.info, policy, **algorithm_params)
   

    # Train
    plotter = PlotDataset(mdp.info, obs_normalized=False)
    core = Core(agent, mdp,callback_step=plotter)
    dataset_eval = core.evaluate(n_episodes=n_steps_test)
    J = compute_J(dataset_eval, gamma=mdp.info.gamma)
    logger.epoch_info(0, J=np.mean(J))#, policy_weights=policy.get_weights().tolist())

    for i in trange(n_epochs, leave=False):
        core.learn(n_episodes=n_steps * n_steps_test,
                   n_episodes_per_fit=n_steps_test)
        dataset_eval = core.evaluate(n_episodes=n_steps_test)
        J = compute_J(dataset_eval, gamma=mdp.info.gamma)
        logger.epoch_info(i+1, J=np.mean(J))#,policy_weights=policy.get_weights().tolist())
    logger.info('Saving plotting and normalization data')
    os.makedirs("./logs/plot_and_norm", exist_ok=True)
    #prepro.save("./logs/plot_and_norm/preprocessor.msh")
    plotter.save_state("./logs/plot_and_norm/plotting_state")
    return agent

def trpo_experiment( n_epochs, n_steps, n_steps_test, n_steps_per_fit):
    np.random.seed(1)
    torch.manual_seed(1)
    logger = Logger( results_dir=None)
    # prepare logging


    # create the policy
    dim_env_state = mdp.info.observation_space.shape[0]
    dim_action = mdp.info.action_space.shape[0]
    policy_params = dict(
        n_features=32,
        use_cuda=False
    )
    beta = Parameter(1e0)
    pi = BoltzmannTorchPolicy(Network,
                              mdp.info.observation_space.shape,
                              (mdp.info.action_space.n,),
                              beta=beta,
                              **policy_params)


    # setup critic
    critic_params = dict(network=Network,
                         optimizer={'class': optim.RMSprop,
                                    'params': {'lr': 1e-3,
                                               'eps': 1e-5}},
                         loss=F.mse_loss,
                         n_features=32,
                         batch_size=64,
                         input_shape=mdp.info.observation_space.shape,
                         output_shape=(1,))

    alg_params =  dict(ent_coeff=0.0,
                       max_kl=.01,
                       lam=.95,
                       n_epochs_line_search=10,
                       n_epochs_cg=100,
                       cg_damping=1e-2,
                       cg_residual_tol=1e-10)

    # Create the agent
    agent = TRPO(mdp_info=mdp.info,policy =pi,critic_params=critic_params,**alg_params)

    # Create Core
    core = Core(agent, mdp)

    # Evaluation
    dataset = core.evaluate(n_steps=n_steps_test, render=False)
    J = compute_J(dataset, gamma_eval)
    logger.epoch_info(0, J=np.mean(J))

    for n in trange(n_epochs):
        core.learn(n_steps=n_steps, n_steps_per_fit=n_steps_per_fit)
        dataset = core.evaluate(n_steps=n_steps_test, render=False)
        J = compute_J(dataset, gamma_eval)
        logger.epoch_info(n+1, J=np.mean(J))

    logger.info('Press a button to visualize acrobot')
    input()
    core.evaluate(n_episodes=5, render=True)
    return agent

def trpo_experiment( n_epochs, n_steps, n_steps_test, n_steps_per_fit):
    np.random.seed(1)
    torch.manual_seed(1)
    logger = Logger( results_dir=None)
    # prepare logging


    # create the policy
    dim_env_state = mdp.info.observation_space.shape[0]
    dim_action = mdp.info.action_space.shape[0]
    policy_params = dict(
        n_features=32,
        use_cuda=False
    )
    beta = Parameter(1e0)
    pi = BoltzmannTorchPolicy(Network,
                              mdp.info.observation_space.shape,
                              (mdp.info.action_space.n,),
                              beta=beta,
                              **policy_params)


    # setup critic
    critic_params = dict(network=Network,
                         optimizer={'class': optim.RMSprop,
                                    'params': {'lr': 1e-3,
                                               'eps': 1e-5}},
                         loss=F.mse_loss,
                         n_features=32,
                         batch_size=64,
                         input_shape=mdp.info.observation_space.shape,
                         output_shape=(1,))

    alg_params =  dict(ent_coeff=0.0,
                       max_kl=.01,
                       lam=.95,
                       n_epochs_line_search=10,
                       n_epochs_cg=100,
                       cg_damping=1e-2,
                       cg_residual_tol=1e-10)

    # Create the agent
    agent = TRPO(mdp_info=mdp.info,policy =pi,critic_params=critic_params,**alg_params)

    # Create Core
    core = Core(agent, mdp)

    # Evaluation
    dataset = core.evaluate(n_steps=n_steps_test, render=False)
    J = compute_J(dataset, gamma_eval)
    logger.epoch_info(0, J=np.mean(J))

    for n in trange(n_epochs):
        core.learn(n_steps=n_steps, n_steps_per_fit=n_steps_per_fit)
        dataset = core.evaluate(n_steps=n_steps_test, render=False)
        J = compute_J(dataset, gamma_eval)
        logger.epoch_info(n+1, J=np.mean(J))

    logger.info('Press a button to visualize acrobot')
    input()
    core.evaluate(n_episodes=5, render=True)
    return agent

def ddpg_experiment(n_epochs, n_steps, n_steps_test):
    np.random.seed()

    logger = Logger( results_dir=None)
   
    # MDP
    horizon = 200
    gamma = 0.99
 

    # Settings
    initial_replay_size = 100
    max_replay_size = 500
    batch_size = 20
    n_features = 80
    warmup_transitions = 1000
    tau = 0.0001
    lr_alpha = 3e-4

    use_cuda = torch.cuda.is_available()

    # Approximator
    actor_input_shape = mdp_cont.info.observation_space.shape
    actor_params = dict(network=ActorNetwork,
                        n_features=n_features,
                        input_shape=actor_input_shape,
                        output_shape=mdp_cont.info.action_space.shape)

    actor_optimizer = {'class': optim.Adam,
                       'params': {'lr': 3e-4}}

    critic_input_shape =(actor_input_shape[0] + mdp_cont.info.action_space.shape[0],)

    critic_params = dict(network=CriticNetwork,
                         optimizer={'class': optim.Adam,
                                    'params': {'lr': 1e-4}},
                         loss=F.mse_loss,
                         n_features=n_features,
                         input_shape=critic_input_shape,
                          output_shape=(1,))

    beta = Parameter(1e0)
    #policy_class = BoltzmannTorchPolicy
    #policy_params = dict( input_shape = mdp.info.observation_space.shape , output_shape =  mdp.info.action_space.shape, beta=beta)#sigma=np.ones(1) * .2, theta=.15, dt=1e-2)
    approximator = Regressor(LinearApproximator,
                             input_shape=mdp_cont.info.observation_space.shape,
                             output_shape=mdp_cont.info.action_space.shape,)
        
    policy_class = OrnsteinUhlenbeckPolicy
    policy_params = dict(sigma=np.ones(1)*0.4, theta=.15, dt=1e-2)

    # Agent
    agent = DDPG(mdp_cont.info, policy_class, policy_params,
                actor_params, actor_optimizer, critic_params, batch_size, initial_replay_size,
                max_replay_size,0.0001)

    # Algorithm
    core = Core(agent, mdp_cont)
    core.learn(n_steps=initial_replay_size, n_steps_per_fit=initial_replay_size)

    # RUN
    dataset = core.evaluate(n_steps=n_steps_test, render=False)
    s, *_ = parse_dataset(dataset)

    J = np.mean(compute_J(dataset, mdp_cont.info.gamma))
    R = np.mean(compute_J(dataset))
    #E = agent.policy.entropy(s)

    logger.epoch_info(0, J=J, R=R)#, entropy=E)

    core.learn(n_steps=initial_replay_size, n_steps_per_fit=initial_replay_size)

    for n in trange(n_epochs, leave=False):
        core.learn(n_steps=n_steps, n_steps_per_fit=1)
        dataset = core.evaluate(n_steps=n_steps_test, render=False)
        s, *_ = parse_dataset(dataset)

        J = np.mean(compute_J(dataset, mdp_cont.info.gamma))
        R = np.mean(compute_J(dataset))
        
        logger.epoch_info(n+1, J=J, R=R)#, entropy=E)

    logger.info('Press a button to visualize pendulum')
    input()
    core.evaluate(n_episodes=5, render=True)
    return agent
#mdp= mdp_cont
print(mdp.env.grid)  
print(mdp.env.units)
#agent = ddpg_experiment(n_epochs=20, n_steps=2000, n_steps_test=2000)
#agent = a2c_experiment(n_epochs=10, n_steps=10000,n_steps_per_fit=5 ,n_step_test=2000)
agent = rainbow_experiment(max_steps_training=100000, evaluation_frequency_training=1000 ,n_episodes_test=10,params=None,path= (pathlib.Path(__file__).resolve().parents[0] / "results" / pathlib.Path(__file__).stem / datetime.datetime.now().strftime("%Y-%m-%d--%H-%M-%S")).__str__() )
#agent = dqn_experiment(n_epochs=9, n_steps=2000 ,n_steps_test=100)
#agent = ddqn_experiment(n_epochs=10, n_steps=5000 ,n_steps_test=100)
#agent = vpg_experiment(n_epochs=100, n_steps=100, n_steps_test=50)
#agent = ppo_experiment(n_epochs=15, n_steps=1000,n_steps_per_fit=5 ,n_steps_test=2000)
#agent = trpo_experiment( n_epochs=20, n_steps=10000, n_steps_test=200, n_steps_per_fit=5000)
#agent= sac_experiment(n_epochs=100, n_steps=2000, n_steps_test=2000)
#mdp = mdp_cont
#sum_rewards = 0.0
#for _ in range(10):
#    sum_rewards = 0.0
#    states = mdp.reset()
#    terminal = False
#    while not terminal:
#        print(states)

#        action = agent.draw_action(states)
#        states, reward, terminal,_ = mdp.step(action)
#        sum_rewards += reward
#        print(reward)
#        print("action: ", action)
#        print(mdp.env.grid_of_current_time_step)    

#    print(mdp.env.grid)  
#    print(sum_rewards)
#print('Mean evaluation return:', sum_rewards / 100.0)
