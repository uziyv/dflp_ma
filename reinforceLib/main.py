from rl.agents import DQN, VPG, Rainbow
from rl.parameters import StepDecay, LinearDecay
import tensorflow as tf
from rl.layers.preprocessing import MinMaxScaling
from rl.presets import Preset
from rl import utils
from multiprocessing import freeze_support
import numpy as np
import gym as gym

from gym.envs.registration import register
import flp_environment
from flp_environment.wrappers.continous_action_space import ContinousActionSpaceWrapper
from flp_environment.wrappers.discrete_action_space import DiscreteActionSpaceWrapper
import glob
import os
import shlex
import subprocess
import yaml
import multiprocessing as mp

import sys

LEFT = 0
DOWN = 1
RIGHT = 2
UP = 3
NOTHING = 4

ROTATE90 = 0
ROTATE180 = 1
ROTATE270 = 2
ROTATE360 = 3

EMPTY = 0
OBSTACLE = -1
FLOW = -2
START_OF_FLOW = -3
END_OF_FLOW = -4


EMPTY = 0
OBSTACLE = -1
FLOW = -2
START_OF_FLOW = -3
END_OF_FLOW = -4

selected_agent = "ppo"
'''
desc = [[[EMPTY, EMPTY, EMPTY, START_OF_FLOW,EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY,EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY,EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, START_OF_FLOW, EMPTY,OBSTACLE, EMPTY, OBSTACLE]],
        [[OBSTACLE, OBSTACLE, EMPTY, EMPTY,EMPTY, EMPTY, OBSTACLE],
        [EMPTY, EMPTY, EMPTY, EMPTY,EMPTY, EMPTY, START_OF_FLOW],
        [EMPTY, EMPTY, EMPTY, EMPTY,EMPTY, OBSTACLE, EMPTY],
        [END_OF_FLOW, EMPTY, EMPTY, EMPTY,EMPTY, EMPTY, OBSTACLE]],

        [[START_OF_FLOW, EMPTY, EMPTY, EMPTY,EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY,EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY,EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY,EMPTY, EMPTY, END_OF_FLOW]]
                ]
## scenario 
desc=[[[OBSTACLE,OBSTACLE,START_OF_FLOW,EMPTY,EMPTY],
       [OBSTACLE,OBSTACLE,EMPTY,EMPTY,EMPTY],
        [EMPTY,EMPTY,EMPTY,OBSTACLE,OBSTACLE],
        [EMPTY,EMPTY,END_OF_FLOW,EMPTY,EMPTY]
       ]
      ,[[OBSTACLE,OBSTACLE,START_OF_FLOW,EMPTY,EMPTY],
       [OBSTACLE,OBSTACLE,EMPTY,EMPTY,EMPTY],
        [EMPTY,EMPTY,EMPTY,OBSTACLE,OBSTACLE],
        [EMPTY,EMPTY,END_OF_FLOW,EMPTY,EMPTY]]]
units = [np.asarray([[1, 2],[EMPTY,2]], dtype= np.dtype(int)), np.asarray(
    [[4, 5],[5,5]], dtype= np.dtype(int))]

## scenario 
units =  [[[EMPTY, 2],[2,1]], 
   [[EMPTY, 5],[5,4]]]

initial_positions= {0:[[0,0]], 1:[[2,4]]}

fix_cost = 1

material_flow = [[0,1],[1,0],[0,1]]
## scenario 
material_flow = [[1,0],[0,1],[0,1]]
##
#material_flow = [[[1,0]],[[0,1]]]
interarrival_time=[10,100]
register(  id='flp_discrete-v1',
          entry_point='flp_environment.envs:FLP_Environment_discrete',kwargs= {"desc": desc, "units" : units,"material_flow":material_flow, "fix_cost":fix_cost, "initial_positions":initial_positions,"period_length" :8, "interarrival_time": interarrival_time},) 
register(id = "flp_cont-v1", 
          entry_point='flp_environment.envs:FLP_Environment_Continous')'''


with open('config.yaml', 'r') as file:
    config = yaml.safe_load(file)
with open('env_config.yaml', 'r') as file:
    env_config = yaml.safe_load(file)

agent_seed = config["general"]["agent_seed"]


def prepare_units(units):
    units_copy = np.copy(units)
    for i in range(0, len(units_copy)):
        unit = units_copy[i]
        for j in range(0, len(unit)):
            for k in range(0, len(unit[0])):
                unit[j][k] = unit[j][k]+i*3 if unit[j][k] != 0 else 0

    return units_copy


def make_env():
    grids = env_config["grids"]
    units = prepare_units(env_config["units"])
    material_flow = env_config["material_flow"]
    fix_cost = env_config["fix_cost"]
    period = env_config["period"]
    interarrival_time = env_config["interarrival_time"]
    initial_positions = env_config["initial_positions"]
    env = gym.make('flp_discrete-v1', desc=grids, units=units, material_flow=material_flow, fix_cost=fix_cost,
                   interarrival_time=interarrival_time, period_length=period, initial_positions=initial_positions)  # env = ObservationNormalizerWrapper(env)
    # env = NormalizeReward(env)
    # env = TimeLimit(env=env, max_episode_steps=len(units)*len(grids))
    return env


env = DiscreteActionSpaceWrapper(make_env())
env_cont = ContinousActionSpaceWrapper(make_env)
# env_cont= gym.make('flp_cont-v1', desc= desc, units = units,material_flow=material_flow, fix_cost=fix_cost, initial_positions=initial_positions,period_length =8, interarrival_time= interarrival_time)
# Fix random seed
seed = config["general"]["agent_seed"]
utils.set_random_seed(seed)

# Preprocess states by min-max scaling, scaling then in [-1, 1]
scaler = MinMaxScaling(min_value=-3,
                       max_value=5)
tf.device("cuda")
# Create the DQN agent on CartPole
'''
dqn = DQN(env=env, name='dqn-cartpole', batch_size=512,
          policy='e-greedy', lr=1e-3, update_target_network=100,
          # halve epsilon each 100 episodes
          epsilon=LinearDecay(initial_value=1., end_value=0.1, steps=1000), gamma=0.99,
          memory_size=5000, seed=utils.GLOBAL_SEED,
          # set to `False` to disable tensorboard summaries; saved by default in `logs/dqn-cartpole`
          use_summary=True,
          # create two-layer 64 units with ReLU + min-max scaler on states
          network=dict(units=[32, 32], preprocess=dict(state=scaler)))
'''

horizon = config["vpg"]["horizon"]
policy_learning_rate = float(config["vpg"]["policy_learning_rate"])
value_learning_rate = float(config["vpg"]["value_learning_rate"])
policy_net = config["vpg"]["policy_net"]
value_net = config["vpg"]["value_net"]
training_steps = config["vpg"]["training_steps"]

vpg = VPG(env=env, horizon=horizon, policy_lr=policy_learning_rate, value_lr=value_learning_rate, 
          policy=dict(units=policy_net, preprocess=dict(state=scaler)), value=dict(units=value_net, preprocess=dict(state=scaler)))
'''
rainbow = Rainbow(env=env, name='rainbow', batch_size=64,
                  policy='e-greedy', lr=3e-4, update_target_network=100,
                  # halve epsilon each 100 episodes
                  epsilon=0, gamma=0.99,
                  memory_size=5000,
                  # set to `False` to disable tensorboard summaries; saved by default in `logs/dqn-cartpole`
                  use_summary=True, horizon=3,   network=dict(units=[24, 32], preprocess=dict(state=scaler)),
                  # create two-layer 64 units with ReLU + min-max scaler on states
                  v_min=-1000, v_max=160)
'''

if __name__ == '__main__':
        #freeze_support()
        print("start learning")
        # vpg.load()
        # vpg.evaluate(10,4)
        vpg.learn(episodes=training_steps, timesteps=4, save=True, render=False,
              evaluation=dict(episodes=20, freq=10))  # ,exploration_steps=512)´
        
        print("end training")
    
        


