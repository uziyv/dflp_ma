from gym import Env, logger, spaces, utils
import gym
import numpy as np 

class ContinousActionSpaceWrapper(gym.Wrapper):
    def __init__(self, env):
        super().__init__(env)
        self.action_space = spaces.Box(low= -1 , high =1, shape=(3,), dtype=np.float16)
        self.env =env 


    def step(self, action):
        num_rows =self.env.unwrapped.num_rows
        num_cols =self.env.unwrapped.num_cols
        num_rotations= self.env.unwrapped.number_of_rotations
        row = int((action[0]+1)*num_rows/2)
        col = int((action[1]+1)*num_cols/2)
        rotation = int((action[2]+1)*num_rotations/2)
        if row == num_rows : 
            row =num_rows-1 
        if col== num_cols:
            col = num_cols-1
        if rotation ==num_rotations : 
            rotation=num_rotations-1


        action= [row,col,rotation]
       
        obs, reward, terminated, truncated, info = self.env.step(action)
       
        return obs, reward, terminated, truncated, info