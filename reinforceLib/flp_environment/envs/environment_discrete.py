import array
import collections
import random
from gym import Env, logger, spaces, utils
import numpy as np
import pygame
import logging
import copy


ROTATE90 = 1
ROTATE180 = 2
ROTATE270 = 3
ROTATE360 = 0

EMPTY = 0
OBSTACLE = -1

START_OF_FLOW = -2
END_OF_FLOW = -3


"""
E: Empty
O:Obstacle
S: start of stream
F: Flow
Q: end Of Stream

Numbers : units

Coding of units: 

E: Not part of the unit
Numer: Unit 
I: Input of the unit
x: output of the unit 
"""

desc = [[[EMPTY, EMPTY, EMPTY, START_OF_FLOW, EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, START_OF_FLOW, EMPTY, OBSTACLE, EMPTY, OBSTACLE]],
        [[OBSTACLE, OBSTACLE, EMPTY, EMPTY, EMPTY, EMPTY, OBSTACLE],
        [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, START_OF_FLOW],
        [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, OBSTACLE, EMPTY],
        [END_OF_FLOW, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, OBSTACLE]],

        [[START_OF_FLOW, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, END_OF_FLOW]]
        ]
# scenario
desc = [[[OBSTACLE, OBSTACLE, START_OF_FLOW, EMPTY, EMPTY],
         [OBSTACLE, OBSTACLE, EMPTY, EMPTY, EMPTY],
         [EMPTY, EMPTY, EMPTY, OBSTACLE, OBSTACLE],
         [EMPTY, EMPTY, END_OF_FLOW, EMPTY, EMPTY]
         ], [[OBSTACLE, OBSTACLE, START_OF_FLOW, EMPTY, EMPTY],
             [OBSTACLE, OBSTACLE, EMPTY, EMPTY, EMPTY],
             [EMPTY, EMPTY, EMPTY, OBSTACLE, OBSTACLE],
             [EMPTY, EMPTY, END_OF_FLOW, EMPTY, EMPTY]]]
units = [np.asarray([[1, 2], [EMPTY, 2]], dtype=np.dtype(int)), np.asarray(
    [[4, 5], [5, 5]], dtype=np.dtype(int))]

# scenario
units = [[[EMPTY, 2], [2, 1]],
         [[EMPTY, 5], [5, 4]]]

initial_positions = {0: [[0, 0]], 1: [[2, 4]]}

fix_cost = 1

material_flow = [[[0, 1]], [[1, 0]]]
# scenario
material_flow = [[[1, 0]], [[0, 1]]]


##
# material_flow = [[[1,0]],[[0,1]]]
interarrival_time = [[10], [100]]
period = 8


class FLP_Environment_discrete(Env):

    def __init__(self, desc, units, material_flow, initial_positions, period_length, interarrival_time, fix_cost=1,):
        self.original_grid = np.array(desc, dtype=np.dtype(int))
        self.main_flow_index =[]
        for i in range(0,len(interarrival_time)):
            self.main_flow_index.append(np.array(interarrival_time[i]).argmax(axis=0))
        self.main_interarrival_time = []
        for i in range(0,len(interarrival_time)):
            self.main_interarrival_time.append(interarrival_time[i][self.main_flow_index[i]])
        self.main_material_flow= []
        for i in range(0,len(interarrival_time)):
            self.main_material_flow.append(material_flow[i][self.main_flow_index[i]])
        self.interarrival_time = interarrival_time
        self.original_units = units  
        self.material_flow = material_flow
        self.units = units  
        self.grid = desc = np.array(desc, dtype=np.dtype(int))
        self.num_rows, self.num_cols = desc[0].shape
        self.number_of_rotations = 4
        self.nS = self.num_rows * self.num_cols
        self.shape_of_largest_unit = 0
        for unit in units:
            size_of_unit = len(unit)*len(unit[0])
            if size_of_unit > self.shape_of_largest_unit:
                self.shape_of_largest_unit = size_of_unit
        self.n_units = len(units)
        self.n_times_steps = len(desc)
        self.observation_space = spaces.Box(
            low=-4, high=0+len(units)*3, shape=(self.nS + self.shape_of_largest_unit,), dtype=np.dtype(int))
        self.initial_units_position = dict(initial_positions)
        self.original_initial_position = copy.deepcopy(initial_positions)
        self.action_space = spaces.Discrete(self.nS*self.number_of_rotations)
        self.fix_cost = fix_cost
        self.units = units
        self.period_length = period_length
        pygame.init()
        self.cell_size = 125
       
        # setting display size
        self.screen = pygame.display.set_mode(
            (self.num_cols * self.cell_size, self.num_rows * self.cell_size))

    def reset(self,  seed=None, options=None):
        self.initial_units_position = copy.deepcopy(
            self.original_initial_position)
        self.time_step = 0
        self.step_in_one_period = 0
        self.index_of_unit_to_set = self.main_material_flow[self.time_step][self.step_in_one_period]
        self.units_in_last_step = {}
        for i in (0, self.n_units-1):
            self.units_in_last_step[i] = np.copy(self.original_units[i])
        self.grid = np.copy(self.original_grid)
        self.grid_of_current_time_step = self.grid[self.time_step]

        self.state = self.get_state_of_position(self.index_of_unit_to_set)

        return self.state

    def step(self, action):
        #rotation = int(action % self.number_of_rotations)
        #position = int(action / self.number_of_rotations)

        #row = int(position / self.num_cols)
        #col = int(position % self.num_cols)

        row = action[0]
        col =action[1]
        rotation = action[2]
        unit = np.copy(self.units_in_last_step[self.index_of_unit_to_set])


        new_pos = np.array([row, col])

        if rotation == ROTATE90:
            unit = self.rotate_unit(unit, 90)
        elif rotation == ROTATE180:
            unit = self.rotate_unit(unit, 180)
        elif rotation == ROTATE270:
            unit = self.rotate_unit(unit, 270)

        is_valid_pos = self.is_valid_pos(
            self.grid_of_current_time_step, unit, new_pos)

        if is_valid_pos:
            self.place_unit(self.grid_of_current_time_step, unit, new_pos)
            reward = 0
        else:
            reward = -1000

        self.initial_units_position[self.index_of_unit_to_set].append(new_pos)
        if (reward == 0):
            reward = self.get_reward(
                self.grid_of_current_time_step, self.index_of_unit_to_set, unit, new_pos,self.main_material_flow,self.main_interarrival_time)
        
            self.step_in_one_period += 1
            self.units_in_last_step[self.index_of_unit_to_set] = unit

            if (self.step_in_one_period == self.n_units):
                self.time_step += 1

                if (self.time_step < self.n_times_steps):
                    self.grid_of_current_time_step = self.grid[self.time_step]
                    self.step_in_one_period = 0

                    grid_for_collective_reward= self.grid[self.time_step-1]
                    for i in range(0,len(self.material_flow[self.time_step-1])):
                        if i == self.main_flow_index[self.time_step-1]:
                            continue
                        for unit_index in self.material_flow[self.time_step-1][i]:
                            reward += self.get_reward(grid_for_collective_reward,    
                                                      unit_index, self.units_in_last_step[unit_index], self.initial_units_position[unit_index][self.time_step-1],i,self.interarrival_time[self.time_step-1][i])



        if self.step_in_one_period < self.n_units and self.time_step < self.n_times_steps:
            self.index_of_unit_to_set = self.main_material_flow[self.time_step][self.step_in_one_period]
            self.state = self.get_state_of_position(self.index_of_unit_to_set)
   
        done = reward < 0 or self.time_step == self.n_times_steps

        return self.state, reward, done,{}

    def render(self, mode):
        # Clear the screen
        self.screen.fill((255, 255, 255))

        # Draw env elements one cell at a time
        for row in range(self.num_rows):
            for col in range(self.num_cols):
                cell_left = col * self.cell_size
                cell_top = row * self.cell_size

                # Obstacle
                if self.grid_of_current_time_step[row][col] == OBSTACLE:
                    pygame.draw.rect(
                        self.screen, (0, 0, 0), (cell_left, cell_top, self.cell_size, self.cell_size))
                # Starting position
                elif self.grid_of_current_time_step[row][col] == START_OF_FLOW:
                    pygame.draw.rect(
                        self.screen, (0, 255, 0), (cell_left, cell_top, self.cell_size, self.cell_size))
                # Goal position
                elif self.grid_of_current_time_step[row][col] == EMPTY:
                    pygame.draw.rect(
                        self.screen, (255, 255, 255), (cell_left, cell_top, self.cell_size, self.cell_size))
                      # Goal position
                elif self.grid_of_current_time_step[row][col] == END_OF_FLOW:
                    pygame.draw.rect(
                        self.screen, (1, 6, 102), (cell_left, cell_top, self.cell_size, self.cell_size))
                else:
                    pygame.draw.rect(
                        self.screen, (0, 0, 0), (cell_left, cell_top, self.cell_size, self.cell_size))
                    self.screen.blit(pygame.font.SysFont('Arial', 25).render(
                        str(self.grid_of_current_time_step[row][col]), True, (255, 255, 255)), (cell_left, cell_top))

                # Agent position
                '''if np.array_equal(np.array(self.current_pos), np.array([row, col]).reshape(-1, 1)):
                    pygame.draw.rect(
                        self.screen, (0, 0, 255), (cell_left, cell_top, self.cell_size, self.cell_size))'''

        pygame.display.update()  # Update the display

    def rotate_unit(self, unit, degree):

        for i in range(int(degree/90)):
            unit = np.rot90(unit)
        return unit

    def get_shortest_distance_to_goal(self, grid, start, goal):
        queue = collections.deque([[start]])

        seen = set([tuple(start)])
        while queue:
            path = queue.popleft()
            x, y = path[-1]
            if grid[x][y] == (goal):
                return path
            for x2, y2 in ((x+1, y), (x-1, y), (x, y+1), (x, y-1)):
                if 0 <= x2 < self.num_rows and 0 <= y2 < self.num_cols and (np.all(EMPTY == (grid[x2][y2])) or np.all(START_OF_FLOW == (grid[x2][y2])) or np.all(END_OF_FLOW == (grid[x2][y2])) or np.all(goal == (grid[x2][y2]))) and (x2, y2) not in seen:
                    queue.append(path + [(x2, y2)])
                    seen.add((x2, y2))

    def get_shortest_distance_to_other_position(self, grid, start, goal):
        queue = collections.deque([[start]])
        seen = set([tuple(start)])
        while queue:
            path = queue.popleft()
            x, y = path[-1]
            if x == goal[0] and y == goal[1]:
                return path
            for x2, y2 in ((x+1, y), (x-1, y), (x, y+1), (x, y-1)):
                if 0 <= x2 < self.num_rows and 0 <= y2 < self.num_cols and (np.all(EMPTY == (grid[x2][y2])) or np.all(START_OF_FLOW == (grid[x2][y2])) or np.all(END_OF_FLOW == (grid[x2][y2]))) and (x2, y2) not in seen:
                    queue.append(path + [(x2, y2)])
                    seen.add((x2, y2))

    def get_visited_positions_from_start(self, grid, start):
        queue = collections.deque([[start]])
        seen = set([tuple(start)])
        while queue:
            path = queue.popleft()
            x, y = path[-1]
            for x2, y2 in ((x+1, y), (x-1, y), (x, y+1), (x, y-1)):
                if 0 <= x2 < self.num_rows and 0 <= y2 < self.num_cols and (np.all(EMPTY == (grid[x2][y2])) or np.all(START_OF_FLOW == (grid[x2][y2])) or np.all(END_OF_FLOW == (grid[x2][y2]))) and (x2, y2) not in seen:
                    queue.append(path + [(x2, y2)])
                    seen.add((x2, y2))
        return seen

    def is_valid_pos(self, grid, unit, starting_pos):
        if starting_pos[0] < 0 or starting_pos[1] < 0 or starting_pos[0] >= self.num_rows or starting_pos[1] >= self.num_cols:

            return False
        if starting_pos[0]+unit.shape[0]-1 >= self.num_rows or starting_pos[1]+unit.shape[1]-1 >= self.num_cols:

            return False
        for i in range(starting_pos[0], starting_pos[0] + unit.shape[0]):
            for j in range(starting_pos[1], starting_pos[1] + unit.shape[1]):
                if not grid[i][j].__eq__(EMPTY) and not unit[i-starting_pos[0]][j-starting_pos[1]] == (EMPTY):

                    return False
        return True

    def place_unit(self, grid, unit, starting_pos):
        for i in range(starting_pos[0], starting_pos[0] + unit.shape[0]):
            for j in range(starting_pos[1], starting_pos[1] + unit.shape[1]):
                if (grid[i][j].__eq__(EMPTY)):
                    grid[i][j] = unit[i-starting_pos[0]][j-starting_pos[1]]

    def remove_unit(self, grid, unit, starting_pos):
        for i in range(starting_pos[0], starting_pos[0] + unit.shape[0]):
            for j in range(starting_pos[1], starting_pos[1] + unit.shape[1]):
                # wrong remove
                grid[i][j] = EMPTY

    def get_position_of_item(self, grid, input_of_unit):
        for i in range(grid.shape[0]):
            for j in range(grid.shape[1]):
                if (grid[i][j].__eq__(input_of_unit)):
                    return np.asarray([i, j])

    def get_reward(self, grid, index_of_unit, unit, new_pos, flow, interarrival):
        punishment_for_invalid_grid = -1000
        if (not self.still_reachable(grid,flow=flow)):
       
            return punishment_for_invalid_grid

        input_of_unit = index_of_unit*3 + 1
        pos_of_input_of_unit = self.get_position_of_item(
            grid, input_of_unit)

        output_of_unit = index_of_unit*3 + 3
        pos_of_output_of_unit = self.get_position_of_item(
            grid, output_of_unit)

        if pos_of_output_of_unit is None:
            pos_of_output_of_unit = pos_of_input_of_unit

        distance_to_previous = 0
        distance_to_next = 0

        if (self.step_in_one_period == 0):
            path_to_previous = self.get_shortest_distance_to_goal(
                grid, pos_of_input_of_unit, START_OF_FLOW)

        else:
            output_of_pre_unit = flow[self.time_step][self.step_in_one_period-1]*3 + 3
            path_to_previous = self.get_shortest_distance_to_goal(
                grid, pos_of_input_of_unit, output_of_pre_unit)
            if path_to_previous is None:

                output_of_pre_unit = flow[self.time_step][self.step_in_one_period-1]*3 + 1
                path_to_previous = self.get_shortest_distance_to_goal(
                    grid, pos_of_input_of_unit, output_of_pre_unit)

        if not (path_to_previous is None):
            distance_to_previous = len(path_to_previous)-2

        else:
 
            return punishment_for_invalid_grid

        if (self.step_in_one_period == self.n_units-1):

            if not (self.get_position_of_item(grid, END_OF_FLOW) is None):

                path_to_next = self.get_shortest_distance_to_goal(
                    grid, pos_of_output_of_unit, END_OF_FLOW)
            else:

                path_to_next = self.get_shortest_distance_to_goal(
                    grid, pos_of_output_of_unit, START_OF_FLOW)

            if (path_to_next == None):
                return punishment_for_invalid_grid
            else:
                distance_to_next = len(path_to_next)-2

        punishment = 0

        if (self.time_step > 0 and ((not np.all(self.initial_units_position[index_of_unit][self.time_step] == (new_pos)))
                                    or not np.all(self.units_in_last_step[index_of_unit] == unit))):
            punishment = - self.fix_cost
   

        weight = period / interarrival[self.time_step]
        reward = (self.nS-distance_to_previous-weight*distance_to_previous) + \
            (self.nS-distance_to_next-weight*distance_to_next) + punishment
        return reward

    def still_reachable(self, grid,flow):
        size_of_placed_units = 0

        for i in range(0, self.step_in_one_period):

            unit = flow[self.time_step][i]
            input_of_unit = unit*3 + 1
            pos_of_input_of_unit = self.get_position_of_item(
                grid, input_of_unit)

            output_of_unit = unit*3 + 3
            pos_of_output_of_unit = self.get_position_of_item(
                grid, output_of_unit)

            if (i == 0):
                path_to_previous = self.get_shortest_distance_to_goal(
                    grid, pos_of_input_of_unit, START_OF_FLOW)

            else:
                output_of_pre_unit = flow[self.time_step][i-1]*3 + 3
                path_to_previous = self.get_shortest_distance_to_goal(
                    grid, pos_of_input_of_unit, output_of_pre_unit)

            if (path_to_previous == None):
                return False

            if (i == len(self.units)-1):
                if not (self.get_position_of_item(grid, END_OF_FLOW) is None):

                    path_to_next = self.get_shortest_distance_to_goal(
                        grid, pos_of_output_of_unit, END_OF_FLOW)
                else:
                    path_to_next = self.get_shortest_distance_to_goal(
                        grid, pos_of_output_of_unit, START_OF_FLOW)
                if (path_to_next == None):
                    return False
        return True

    def get_state_of_position(self,  unit):
        unit = np.ndarray.flatten(
            np.array(self.original_units[self.index_of_unit_to_set]))
        padded_unit = np.zeros(self.shape_of_largest_unit)
        # np.ndarray.flatten(self.original_units[self.index_of_unit_to_set])
        padded_unit[:unit.shape[0]] = unit
        state = np.append(np.ndarray.flatten(
            self.grid_of_current_time_step), padded_unit)
        state= np.array(state,dtype=np.dtype(int))
        return state


def test_set_unit_at_invalid_position():
    row = 0
    col = 2
    rotation = 0
    position = row*env.num_rows + col*env.num_cols + rotation
    states, reward, terminal, _ = env.step(position)
    print(env.grid)
    print(reward == -1000)


def test_set_unit_at_valid_position():
    for i in range(env.num_rows):
        for j in range(env.num_cols):
            env.reset(1)
            row = i
            col = j
            rotation = 3
            position = i*env.num_cols*env.number_of_rotations + \
                j*env.number_of_rotations + rotation
            states, reward, terminal, _, _ = env.step(position)
            print(i, ",", env.num_cols, ",", env.number_of_rotations)
            print(i, ",", j)
            print(position)
            print(env.grid)
            print(reward)
            print()


def test():
    env.reset()
    test_set_unit_at_invalid_position()
    env.reset()
    test_set_unit_at_valid_position()


def test_1():
    env.reset(1)
    cul_reward = 0
    row = 0
    col = 3
    rotation = 2
    position = row*env.num_cols*env.number_of_rotations + \
        col*env.number_of_rotations + rotation
    states, reward, terminal, _, _ = env.step(position)
    cul_reward += reward
    row = 2
    col = 0
    rotation = 1

    position_2 = row*env.num_cols*env.number_of_rotations + \
        col*env.number_of_rotations + rotation

    states, reward, terminal, _, _ = env.step(position_2)
    cul_reward += reward
    states, reward, terminal, _, _ = env.step(position-1)
    cul_reward += reward
    states, reward, terminal, _, _ = env.step(position_2+2)
    cul_reward += reward
    print(position)
    print(env.grid)
    print(cul_reward)


if __name__ == "__main__":

    env = FLP_Environment_discrete(desc=desc, units=units, fix_cost=fix_cost, initial_positions=initial_positions, 
                                   interarrival_time=interarrival_time, period_length=8, material_flow=material_flow)

    test_1()
  
    # test_set_unit_at_valid_position()
