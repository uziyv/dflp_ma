import collections
import random
from gym import Env, logger, spaces, utils
import numpy as np
import pygame
import logging

LEFT = 0
DOWN = 1
RIGHT = 2
UP = 3
NOTHING = 4

ROTATE90 = 0
ROTATE180 = 1
ROTATE270 = 2
ROTATE360 = 3

EMPTY = 0
OBSTACLE = -1
FLOW = -2
START_OF_FLOW = -3
END_OF_FLOW = -4


"""
E: Empty
O:Obstacle
S: start of stream
F: Flow
Q: end Of Stream

Numbers : units

Coding of units: 

E: Not part of the unit
Numer: Unit 
I: Input of the unit
x: output of the unit 
"""

desc = [[[EMPTY, EMPTY, EMPTY, START_OF_FLOW, EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, END_OF_FLOW, EMPTY, EMPTY, EMPTY, EMPTY]]]
''',

        [[EMPTY, EMPTY, EMPTY, START_OF_FLOW, EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, END_OF_FLOW, EMPTY, EMPTY, EMPTY, EMPTY]],

        [[EMPTY, EMPTY, EMPTY, START_OF_FLOW, EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, FLOW, EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, FLOW, FLOW, EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, END_OF_FLOW, EMPTY, EMPTY, EMPTY, EMPTY]]
        ]'''
#units = [np.asarray([[1, 3]], dtype=np.dtype(int)), np.asarray(
#    [[4, 6]], dtype=np.dtype(int))]
units = [np.asarray([[1, 3],[EMPTY,2]], dtype= np.dtype(int)), np.asarray(
    [[4, 6],[5,5]], dtype= np.dtype(int))]
initial_units_position = np.asarray([np.asarray([0, 0]), np.asarray([0, 2])])


class FLP_Environment_V2(Env):

    def __init__(self, desc, units):
        self.original_grid = np.array(desc, dtype=np.dtype(int))
        self.original_units = np.array(units, dtype=np.dtype(int))
        self.units = np.array(units, dtype=np.dtype(int))
        self.grid = desc = np.array(desc, dtype=np.dtype(int))
        self.num_rows, self.num_cols = num_rows, num_cols = desc[0].shape
        nA_movement = 5
        nA_rotation = 4
        nS = num_rows * num_cols
        shape_of_largest_unit = 4
        self.n_units = len(units)
        self.n_times_steps = len(desc)
        self.observation_space = spaces.Box(
            low=-4, high=0+len(units)*3, shape=(nS + shape_of_largest_unit,), dtype=np.dtype(int))

        #self.action_space = spaces.Box(low=0, high =np.array([nS, nA_rotation]),shape=(2, ),dtype=np.int16)
        self.action_space = spaces.MultiDiscrete([nS, nA_rotation])
        # self.current_pos = np.asarray([random.randint(0, num_rows-1), random.randint(0, num_cols-1)])

        self.units = units
        # Initialize Pygame
        pygame.init()
        self.cell_size = 125
        self.unit_to_set = 0
        self.last_unit_set = 0
        self.time_step = 0
        self.grid_to_use = self.grid[self.time_step]
        self.current_pos = initial_units_position[self.unit_to_set]
        #self.place_unit(self.grid_to_use,
        #               units[self.unit_to_set], self.current_pos)

        self.state = self.get_state_of_position(
            self.current_pos, self.unit_to_set)

        print(self.state)

        # setting display size
        self.screen = pygame.display.set_mode(
            (self.num_cols * self.cell_size, self.num_rows * self.cell_size))

    def reset(self):
        # self.current_pos = np.asarray([random.randint(0, self.num_rows-1), random.randint(0, self.num_cols-1)])

        self.unit_to_set = 0
        self.last_unit_set = 0
        self.current_pos = initial_units_position[self.unit_to_set]
        self.time_step = 0
        self.units=np.copy(self.original_units)
        self.grid = np.copy(self.original_grid)
        self.grid_to_use = self.grid[self.time_step]

        self.state = self.get_state_of_position(
            self.current_pos, self.unit_to_set)
        #self.place_unit(self.grid_to_use,
        #               units[self.unit_to_set], self.current_pos)
        return self.state

    def step(self, action):
        # Move the agent based on the selected action
        #print(action)
        row = int(action[0] / self.num_cols)
        col = int(action[0] % self.num_cols)
        unit = self.units[self.unit_to_set]
        previous_unit = np.copy(self.units[self.last_unit_set])
        if (self.last_unit_set != self.unit_to_set):
        #    self.current_pos = initial_units_position[self.unit_to_set]
        #    while not self.is_valid_pos(self.grid_to_use, unit, self.current_pos):
        #        #print("new pos")
        #        #print(self.grid_to_use)
        #        self.current_pos = np.asarray(
        #            [random.randint(0, self.num_rows), random.randint(0, self.num_cols)])
        #    #self.place_unit(self.grid_to_use, unit, self.current_pos)
        #    #print(self.grid_to_use)
            previous_unit = np.copy(self.units[self.unit_to_set])
            self.last_unit_set = self.unit_to_set

        new_pos = np.array([row, col])
       
        if action[1] == 0:
            unit = self.rotate_unit(unit, 90)
        elif action[1] == 1:
            unit = self.rotate_unit(unit, 180)
        elif action[1] == 2:
            unit = self.rotate_unit(unit, 270)

        #print(unit)
        #print(new_pos)
        #print(self.grid)
        # Check if the new position is valid
        is_valid_pos = self.is_valid_pos(self.grid_to_use, unit, new_pos)

        if is_valid_pos:

            #self.units[self.unit_to_set] = unit
            #if (self.last_unit_set == self.unit_to_set):
            #    self.remove_unit(self.grid_to_use,
            #                     previous_unit, self.current_pos)
            self.current_pos = new_pos
            self.last_unit_set = self.unit_to_set
            self.unit_to_set += 1
            self.place_unit(self.grid_to_use, unit, new_pos)
            #print("grid_after setting \n", self.grid_to_use)
            if self.unit_to_set < self.n_units:
                self.state = self.get_state_of_position(
                    self.current_pos, self.unit_to_set)
            reward = 0
        else:
            reward = -1000      

            #elif self.last_unit_set == self.unit_to_set:
                #units[self.unit_to_set] = previous_unit

        input_of_unit = self.last_unit_set*3 + 1
        pos_of_input_of_unit = self.get_position_of_item(
            self.grid_to_use, input_of_unit)

        output_of_unit = self.last_unit_set*3 + 3
        pos_of_output_of_unit = self.get_position_of_item(
            self.grid_to_use, output_of_unit)

        # Reward function
        '''if np.array_equal(self.current_pos, [0, 0]):
            reward = 1.0

        else:
            reward = 0.0'''
        
        
        if (self.unit_to_set != self.last_unit_set):
          
            reward = self.get_reward(self.grid_to_use, self.last_unit_set, new_pos)

            if(self.unit_to_set == self.n_units):
                self.time_step += 1
                if (self.time_step < self.n_times_steps):
                    self.grid_to_use = self.grid[self.time_step]
                    self.unit_to_set = 0

            
        done = reward < 0 or self.time_step == self.n_times_steps

        return self.state, reward, done, {}

    def render(self):
        # Clear the screen
        self.screen.fill((255, 255, 255))

        # Draw env elements one cell at a time
        for row in range(self.num_rows):
            for col in range(self.num_cols):
                cell_left = col * self.cell_size
                cell_top = row * self.cell_size

                '''try:
                    print(np.array(self.current_pos) ==
                          np.array([row, col]).reshape(-1, 1))
                except Exception as e:
                    print('Initial state')'''
                if self.grid_to_use[row][col] == OBSTACLE:  # Obstacle
                    pygame.draw.rect(
                        self.screen, (0, 0, 0), (cell_left, cell_top, self.cell_size, self.cell_size))
                elif self.grid_to_use[row][col] == START_OF_FLOW:  # Starting position
                    pygame.draw.rect(
                        self.screen, (0, 255, 0), (cell_left, cell_top, self.cell_size, self.cell_size))
                elif self.grid_to_use[row][col] == EMPTY:  # Goal position
                    pygame.draw.rect(
                        self.screen, (255, 255, 255), (cell_left, cell_top, self.cell_size, self.cell_size))
                elif self.grid_to_use[row][col] == FLOW:  # Goal position
                    pygame.draw.rect(
                        self.screen, (94, 94, 94), (cell_left, cell_top, self.cell_size, self.cell_size))
                elif self.grid_to_use[row][col] == END_OF_FLOW:  # Goal position
                    pygame.draw.rect(
                        self.screen, (1, 6, 102), (cell_left, cell_top, self.cell_size, self.cell_size))
                else:
                    pygame.draw.rect(
                        self.screen, (0, 0, 0), (cell_left, cell_top, self.cell_size, self.cell_size))
                    self.screen.blit(pygame.font.SysFont('Arial', 25).render(
                        str(self.grid_to_use[row][col]), True, (255, 255, 255)), (cell_left, cell_top))

                # Agent position
                if np.array_equal(np.array(self.current_pos), np.array([row, col]).reshape(-1, 1)):
                    pygame.draw.rect(
                        self.screen, (0, 0, 255), (cell_left, cell_top, self.cell_size, self.cell_size))

        pygame.display.update()  # Update the display

    def rotate_unit(self, unit, degree):

        for i in range(int(degree/90)):
            unit = np.rot90(unit)
        return unit

    def get_shortest_distance_to_goal(self, grid, start, goal):
        queue = collections.deque([[start]])
        seen = set([tuple(start)])
        while queue:
            path = queue.popleft()
            x, y = path[-1]
            if grid[x][y] == (goal):
                return path
            for x2, y2 in ((x+1, y), (x-1, y), (x, y+1), (x, y-1)):
                if 0 <= x2 < self.num_rows and 0 <= y2 < self.num_cols and (np.all(EMPTY == (grid[x2][y2])) or np.all(goal == (grid[x2][y2]))) and (x2, y2) not in seen:
                    queue.append(path + [(x2, y2)])
                    seen.add((x2, y2))

    def get_shortest_distance_to_other_position(self, grid, start, goal):
        queue = collections.deque([[start]])
        seen = set([tuple(start)])
        while queue:
            path = queue.popleft()
            x, y = path[-1]
            if x == goal[0] and y == goal[1]:
                return path
            for x2, y2 in ((x+1, y), (x-1, y), (x, y+1), (x, y-1)):
                if 0 <= x2 < self.num_rows and 0 <= y2 < self.num_cols and (x2, y2) not in seen:
                    queue.append(path + [(x2, y2)])
                    seen.add((x2, y2))

    def is_valid_pos(self, grid, unit, starting_pos):
        if starting_pos[0] < 0 or starting_pos[1] < 0 or starting_pos[0] >= self.num_rows or starting_pos[1] >= self.num_cols:
            #print("cause_1")
            return False
        if starting_pos[0]+unit.shape[0]-1 >= self.num_rows or starting_pos[1]+unit.shape[1]-1 >= self.num_cols:
            #print(starting_pos[0]+unit.shape[0]-1 )
            #print(starting_pos[1]+unit.shape[1]-1)
            return False
        for i in range(starting_pos[0], starting_pos[0] + unit.shape[0]):
            for j in range(starting_pos[1], starting_pos[1] + unit.shape[1]):
                if not grid[i][j].__eq__(EMPTY) and not unit[i-starting_pos[0]][j-starting_pos[1]] == (EMPTY):
                    #print("i: ", i)
                    #print("j: ", j)
                    #print("value: ", grid[i][j])
                    #print("grid \n", grid)
                    return False
        return True

    def place_unit(self, grid, unit, starting_pos):
        for i in range(starting_pos[0], starting_pos[0] + unit.shape[0]):
            for j in range(starting_pos[1], starting_pos[1] + unit.shape[1]):
                if(grid[i][j].__eq__(EMPTY)):
                    grid[i][j] = unit[i-starting_pos[0]][j-starting_pos[1]]

    def remove_unit(self, grid, unit, starting_pos):
        for i in range(starting_pos[0], starting_pos[0] + unit.shape[0]):
            for j in range(starting_pos[1], starting_pos[1] + unit.shape[1]):
                grid[i][j] = EMPTY

    def get_position_of_item(self, grid, input_of_unit):
        for i in range(grid.shape[0]):
            for j in range(grid.shape[1]):
                if (grid[i][j].__eq__(input_of_unit)):
                    return np.asarray([i, j])

    def get_reward(self, grid, unit, new_pos):
        if (not self.still_reachable(grid)):
            return -1000

        input_of_unit = unit*3 + 1
        pos_of_input_of_unit = self.get_position_of_item(
            grid, input_of_unit)

        output_of_unit = unit*3 + 3
        pos_of_output_of_unit = self.get_position_of_item(
            grid, output_of_unit)

        distance_to_previous = 0
        distance_to_next = 0

        if (unit == 0):
            path_to_previous = self.get_shortest_distance_to_goal(
                    grid, pos_of_input_of_unit, START_OF_FLOW)

        else:
            output_of_pre_unit = (unit-1)*3 + 3
            path_to_previous = self.get_shortest_distance_to_goal(
                    grid, pos_of_input_of_unit, output_of_pre_unit)

        if (path_to_previous != None):
            distance_to_previous = len(path_to_previous)

        else:
            distance_to_previous = 1000

        if (unit == len(self.units)-1):
            path_to_next = self.get_shortest_distance_to_goal(
                    grid, pos_of_output_of_unit, END_OF_FLOW)
            if (path_to_next == None):
                distance_to_next = 1000
            else:
                distance_to_next = len(path_to_next)

        nS = self.num_cols * self.num_rows
        distance_from_original = 0
        if (not np.all(initial_units_position[unit] == (new_pos))):
            distance_from_original = len(self.get_shortest_distance_to_other_position(
                grid, initial_units_position[unit], new_pos))
        punishment = - distance_from_original
        logging.info("distance to previous: ", distance_to_previous)
        logging.info("distance to next: ", distance_to_next)
        reward = (nS-distance_to_previous) + \
                (nS-distance_to_next) + punishment
        return reward
    
    def still_reachable(self, grid):
        for unit in range(0, self.unit_to_set):
            input_of_unit = unit*3 + 1
            pos_of_input_of_unit = self.get_position_of_item(
                grid, input_of_unit)

            output_of_unit = unit*3 + 3
            pos_of_output_of_unit = self.get_position_of_item(
                grid, output_of_unit)



            if (unit == 0):
                path_to_previous = self.get_shortest_distance_to_goal(
                    grid, pos_of_input_of_unit, START_OF_FLOW)

            else:
                output_of_pre_unit = (unit-1)*3 + 3
                path_to_previous = self.get_shortest_distance_to_goal(
                    grid, pos_of_input_of_unit, output_of_pre_unit)

            if (path_to_previous == None):
                return False

            if (unit == len(self.units)-1):
                path_to_next = self.get_shortest_distance_to_goal(
                    grid, pos_of_output_of_unit, END_OF_FLOW)
                if (path_to_next == None):
                    return False
        
        return True


    def get_state_of_position(self, pos, unit):
        '''path_to_flow = self.get_shortest_distance_to_goal(self.desc, pos, FLOW)
        path_to_input = self.get_shortest_distance_to_goal(self.desc, pos, START_OF_FLOW)
        path_to_output= self.get_shortest_distance_to_goal(self.desc, pos, END_OF_FLOW)
        if(path_to_input != None):
            distance_to_input = len(path_to_input)
        else:
            distance_to_input = np.inf
        if(path_to_input != None):
            distance_to_output = len(path_to_output)
        else:
            distance_to_output = np.inf
        if(path_to_flow!=None):
            distance_to_flow = len(path_to_flow)
        else:
            distance_to_flow = np.inf
        print("pos: ", pos)
        print("path to flow : ", path_to_flow)
        direction_to_flow = self.get_direction_to_flow(
            tuple(map( lambda i, j: j  - i  , pos ,np.asarray(path_to_flow[1]))))
        cost_to_transfer = 0
        unit_to_set = unit
        distance_to_previous_unit=0
        if(self.last_unit_set > 0 ):
            previous_unit = self.last_unit_set-1
            distance_to_previous_unit = len(self.get_shortest_distance_to_goal(self.desc, pos, "X"+ str(previous_unit)))
        obs = [distance_to_input, distance_to_output, distance_to_flow,
               direction_to_flow, cost_to_transfer, unit_to_set, distance_to_previous_unit]
        return np.asarray(obs)'''

        return np.append(np.ndarray.flatten(self.grid_to_use), np.ndarray.flatten(self.original_units[self.unit_to_set]))

    def get_direction_to_flow(self, direction):
        print("direction: ", direction)
        if (direction == (1, 0)):
            return DOWN
        elif (direction == (-1, 0)):
            return UP
        elif (direction == (0, 1)):
            return RIGHT
        elif (direction == (0, -1)):
            return LEFT
        return None


'''env = FLP_Environment_V2(desc, units)
obs = env.reset()
env.render()

done = False
while not done:
    pygame.event.get()
    print(env.state)
    action = env.action_space.sample()  # Random action selection
    obs, reward, done, _ = env.step(action)
    env.render()
    print('Reward:', reward)
    print('Done:', done)
    print(env.desc)

    pygame.time.wait(200)

'''
