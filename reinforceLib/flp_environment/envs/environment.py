import collections
import random
from gym import Env, logger, spaces, utils
import numpy as np
import pygame


LEFT = 0
DOWN = 1
RIGHT = 2
UP = 3
NOTHING = 4

ROTATE90 = 0
ROTATE180 = 1
ROTATE270 = 2
ROTATE360 = 3

EMPTY = "E "
OBSTACLE = "O "
START_OF_FLOW = "S "
FLOW = "F "
END_OF_FLOW = "Q "


"""
E: Empty
O:Obstacle
S: start of stream
F: Flow
Q: end Of Stream

Numbers : units

Coding of units: 

E: Not part of the unit
Numer: Unit 
I: Input of the unit
x: output of the unit 
"""

desc = [[EMPTY, EMPTY, EMPTY, START_OF_FLOW],
                [EMPTY, EMPTY, EMPTY, FLOW],
                [EMPTY, EMPTY, FLOW, FLOW],
                [EMPTY, EMPTY, END_OF_FLOW, EMPTY]]
units = [np.asarray([["I0", "X0"]], dtype= np.dtype(str)), np.asarray(
    [["I1", "X1"]], dtype= np.dtype(str))]
print(desc)
initial_units_position = [[1, 0], [1, 1]]


class FLP_Environment(Env):

    def __init__(self, desc, units):

        self.desc = desc = np.array(desc, dtype=np.dtype(str))
        self.num_rows, self.num_cols = num_rows, num_cols = desc.shape
        nA_movement = 5
        nA_rotation = 4
        nS = num_rows * num_cols
        n_units = len(units)
        self.observation_space = spaces.Box(low=np.asarray([0, 0, 0, 1, 0, 0, 0]), high=np.asarray([
                                            nS, nS, nS, 4, 1, n_units-1, nS]), dtype=np.int32)
        '''    self.observation_space = spaces.Dict(
            {
                "agent": spaces.Box([0, 0], [num_rows-1,.num_cols-1],  dtype=int),
                "distanceToInput": spaces.Discrete(num_rows.num_cols),
                "distanceToOutput": spaces.Discrete(num_rows.num_cols),
                "distanceToFlow": spaces.Discrete(num_rows.num_cols),
                "directionToFlow": spaces.Discrete(4),
                "costToTransfer": spaces.Discrete(1),
                "unitToSet": spaces.Discrete(len(units)),
                "distanceToPreviousUnit": spaces.Discrete(num_rows.num_cols),

            }
        )'''
        self.action_space = spaces.MultiDiscrete([nA_movement, nA_rotation])
        # self.current_pos = np.asarray([random.randint(0, num_rows-1), random.randint(0, num_cols-1)])

        self.units = units
        # Initialize Pygame
        pygame.init()
        self.cell_size = 125
        self.unit_to_set = 0
        self.last_unit_set = 0
        self.current_pos = initial_units_position[self.unit_to_set]
        self.place_unit(self.desc, units[ self.unit_to_set], self.current_pos)
        
        self.state = self.get_state_of_position(self.current_pos, self.unit_to_set)
       

        print(self.state)

        # setting display size
        self.screen = pygame.display.set_mode(
            (self.num_cols * self.cell_size, self.num_rows * self.cell_size))

    def reset(self):
        # self.current_pos = np.asarray([random.randint(0, self.num_rows-1), random.randint(0, self.num_cols-1)])
        self.current_pos = initial_units_position[self.unit_to_set]
        self.unit_to_set = 0
        self.last_unit_set = 0
        self.state = self.get_state_of_position(
            self.current_pos, self.unit_to_set)
        self.place_unit(self.desc, units[self.last_unit_set], self.current_pos)
        return self.current_pos

    def step(self, action):
        # Move the agent based on the selected action
        # print(self.state)

        unit = units[self.state[5]]
        previous_unit = np.copy(units[self.last_unit_set])
        if (self.last_unit_set != self.unit_to_set):
            self.current_pos = initial_units_position[self.unit_to_set]
            while not self.is_valid_pos(self.desc, unit, self.current_pos):
                self.current_pos = [random.randint(0,self.num_rows),random.randint(0,self.num_cols)]
            self.place_unit(self.desc, unit, self.current_pos)
            previous_unit = np.copy(units[self.unit_to_set])
            self.last_unit_set = self.unit_to_set


        new_pos = np.array(self.current_pos)
        if action[0] == 0:  # Up
            new_pos[0] -= 1
        elif action[0] == 1:  # Down
            new_pos[0] += 1
        elif action[0] == 2:  # Left
            new_pos[1] -= 1
        elif action[0] == 3:  # Right
            new_pos[1] += 1

        if action[1] == 0:
            unit = self.rotate_unit(unit, 90)
        elif action[1] == 1:
            unit = self.rotate_unit(unit, 180)
        elif action[1] == 2:
            unit = self.rotate_unit(unit, 270)

        # Check if the new position is valid
        is_valid_pos = self.is_valid_pos(self.desc, unit, new_pos)
        if is_valid_pos:

            units[self.state[5]] = unit
            if (self.last_unit_set == self.unit_to_set):
                self.remove_unit(self.desc, previous_unit, self.current_pos)
            self.current_pos = new_pos
            self.last_unit_set = self.unit_to_set
            self.unit_to_set += 1
            self.place_unit(self.desc, unit, new_pos)
            self.state = self.get_state_of_position(self.current_pos, self.unit_to_set)
           
        elif self.last_unit_set == self.unit_to_set:
            units[self.state[5]] = previous_unit

        # Reward function
        if np.array_equal(self.current_pos, [0, 0]):
            reward = 1.0

        else:
            reward = 0.0
        done = self.state[5] >= len(units)

        return self.state, reward, done, {}


    def render(self):
        # Clear the screen
        self.screen.fill((255, 255, 255))

        # Draw env elements one cell at a time
        for row in range(self.num_rows):
            for col in range(self.num_cols):
                cell_left = col * self.cell_size
                cell_top = row * self.cell_size

                '''try:
                    print(np.array(self.current_pos) ==
                          np.array([row, col]).reshape(-1, 1))
                except Exception as e:
                    print('Initial state')'''
                if self.desc[row][ col] ==  OBSTACLE:  # Obstacle
                    pygame.draw.rect(
                        self.screen, (0, 0, 0), (cell_left, cell_top, self.cell_size, self.cell_size))
                elif self.desc[row][ col] == START_OF_FLOW:  # Starting position
                    pygame.draw.rect(
                        self.screen, (0, 255, 0), (cell_left, cell_top, self.cell_size, self.cell_size))
                elif self.desc[row][ col] == EMPTY:  # Goal position
                    pygame.draw.rect(
                        self.screen, (255, 255, 255), (cell_left, cell_top, self.cell_size, self.cell_size))
                elif self.desc[row][ col] == FLOW:  # Goal position
                    pygame.draw.rect(
                        self.screen, (94, 94, 94), (cell_left, cell_top, self.cell_size, self.cell_size))
                elif self.desc[row][ col] == END_OF_FLOW:  # Goal position
                    pygame.draw.rect(
                        self.screen, (1, 6, 102), (cell_left, cell_top, self.cell_size, self.cell_size))
                else:
                    pygame.draw.rect(
                        self.screen, (0, 0, 0), (cell_left, cell_top, self.cell_size, self.cell_size))
                    self.screen.blit(pygame.font.SysFont('Arial', 25).render(
                        self.desc[row][ col], True, (255, 255, 255)), (cell_left, cell_top))

                # Agent position
                if np.array_equal(np.array(self.current_pos), np.array([row, col]).reshape(-1, 1)):
                    pygame.draw.rect(
                        self.screen, (0, 0, 255), (cell_left, cell_top, self.cell_size, self.cell_size))

        pygame.display.update()  # Update the display

    def rotate_unit(self, unit, degree):

        for i in range(int(degree/90)):
            unit = np.rot90(unit)
        return unit

    def get_shortest_distance_to_goal(self, grid, start, goal):
        print(grid)
        queue = collections.deque([[start]])
        seen = set([tuple(start)])
        while queue:
            
            path = queue.popleft()
            x, y = path[-1]
            print("point, ", x , ", ", y )
            if str(grid[x][y])== str(goal):
                return path
            for x2, y2 in ((x+1, y), (x-1, y), (x, y+1), (x, y-1)):
                if 0 <= x2 < self.num_rows and 0 <= y2 < self.num_cols and (EMPTY.__eq__(grid[x2][y2]) or goal.__eq__(grid[x2][y2])) and (x2, y2) not in seen:
                    
                    
                    queue.append(path + [(x2, y2)])
                    seen.add((x2, y2))
    

    def is_valid_pos(self, grid, unit, starting_pos):
        if starting_pos[0] < 0 or starting_pos[1] < 0 or starting_pos[0] >= self.num_rows or starting_pos[1] >= self.num_cols:
            return False
        if starting_pos[0]+unit.shape[0] >= self.num_rows or starting_pos[1]+unit.shape[1] >= self.num_cols:
            return False
        for i in range(starting_pos[0], starting_pos[0] + unit.shape[0]):
            for j in range(starting_pos[1], starting_pos[1] + unit.shape[1]):
                if grid[i][j] != EMPTY and unit[i-starting_pos[0]][j-starting_pos[1]] != EMPTY:
                    return False
        return True

    def place_unit(self, grid, unit, starting_pos):
        for i in range(starting_pos[0], starting_pos[0] + unit.shape[0]):
            for j in range(starting_pos[1], starting_pos[1] + unit.shape[1]):
                grid[i][j] = unit[i-starting_pos[0]][j-starting_pos[1]]

    def remove_unit(self, grid, unit, starting_pos):
        for i in range(starting_pos[0], starting_pos[0] + unit.shape[0]):
            for j in range(starting_pos[1], starting_pos[1] + unit.shape[1]):
                grid[i][j] = EMPTY

    def get_state_of_position(self, pos, unit):
        print("pos: ", pos)
        path_to_flow = self.get_shortest_distance_to_goal(self.desc, pos, FLOW)
        path_to_input = self.get_shortest_distance_to_goal(self.desc, pos, START_OF_FLOW)
        path_to_output= self.get_shortest_distance_to_goal(self.desc, pos, END_OF_FLOW)
        if(path_to_input != None):
            distance_to_input = len(path_to_input)
        else:
            distance_to_input = np.inf
        if(path_to_input != None):
            distance_to_output = len(path_to_output)
        else:
            distance_to_output = np.inf
        if(path_to_flow!=None):
            distance_to_flow = len(path_to_flow)
        else:
            distance_to_flow = np.inf
        print("pos: ", pos)
        print("path to flow : ", path_to_flow)
        direction_to_flow = self.get_direction_to_flow(
            tuple(map( lambda i, j: j  - i  , pos ,np.asarray(path_to_flow[1]))))
        cost_to_transfer = 0
        unit_to_set = unit
        distance_to_previous_unit=0
        if(self.last_unit_set > 0 ):
            previous_unit = self.last_unit_set-1
            distance_to_previous_unit = len(self.get_shortest_distance_to_goal(self.desc, pos, "X"+ str(previous_unit)))
        obs = [distance_to_input, distance_to_output, distance_to_flow,
               direction_to_flow, cost_to_transfer, unit_to_set, distance_to_previous_unit]
        return np.asarray(obs)

    def get_direction_to_flow(self, direction):
        print("direction: ", direction)
        if (direction == (1, 0)):
            return DOWN
        elif (direction == (-1, 0)):
            return UP
        elif (direction == (0, 1)):
            return RIGHT
        elif (direction == (0, -1)):
            return LEFT
        return None

'''
env = Environment(desc, units)
obs = env.reset()
env.render()

done = False
while not done:
    pygame.event.get()
    print(env.state)
    action = env.action_space.sample()  # Random action selection
    obs, reward, done, _ = env.step(action)
    env.render()
    # print('Reward:', reward)
    # print('Done:', done)
    print(env.state)

    pygame.time.wait(200)
'''