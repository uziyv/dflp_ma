import torch as th
import torch.nn as nn
from gymnasium import spaces

from stable_baselines3 import PPO
from stable_baselines3.common.torch_layers import BaseFeaturesExtractor


class CustomCNN(BaseFeaturesExtractor):
    def __init__(self, observation_space: spaces.Box):
        # We do not know features-dim here before going over all the items,
        # so put something dummy for now. PyTorch requires calling
        # nn.Module.__init__ before adding modules
        super().__init__(observation_space, features_dim=1)
        
        total_concat_size = 0
        extractors = {}
        extractors["grid"] = nn.Sequential(
            nn.Conv2d(in_channels=4,out_channels=32,kernel_size=3),   
            nn.ReLU(),
            nn.Conv2d(in_channels=32,out_channels=64,kernel_size=3),   
            nn.ReLU(),
            nn.MaxPool2d(2),
            nn.Conv2d(in_channels=64,out_channels=32,kernel_size=3),   
            nn.ReLU(),
            nn.Conv2d(in_channels=32,out_channels=32,kernel_size=3),   
            nn.ReLU(),
            nn.Flatten())
        total_concat_size += 210 // 4 * 210  // 4
        
        #extractors["infos"] = nn.Linear(21 , 16)
        total_concat_size += 16
      
   
        self.extractors = nn.ModuleDict(extractors)

        # Update the features dim manually
        self._features_dim = 32

    def forward(self, observations) -> th.Tensor:
        encoded_tensor_list = []
        
        observatiosAsList=dict()
        #observatiosAsList["grid"]=list()
        #observatiosAsList["infos"]= list()

        
        obsOfGrid= observations[:,:210]
        obsOfGrid= obsOfGrid.view(-1,1,15,14)
        infos= observations[:,210:]


        #observations=th.swapaxes(observations,1,3)
        #observations=th.swapaxes(observations,2,3)
        
        observatiosAsList["grid"]= obsOfGrid
        observatiosAsList["infos"]= infos

       
        # self.extractors contain nn.Modules that do all the processing.
        for  key,extractor in self.extractors.items():
            #encoded_tensor_list.append(extractor(observatiosAsList[key]))
       
            encoded_tensor_list.append( extractor(observations))
        # Return a (B, self._features_dim) PyTorch tensor, where B is batch dimension.
       
        features = th.cat(encoded_tensor_list, dim=1)
        #print(features)
        return features