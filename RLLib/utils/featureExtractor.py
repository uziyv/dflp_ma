import torch as th
import torch.nn as nn
from gymnasium import spaces




class FeatureExtractor():
    """
    :param observation_space: (gym.Space)
    :param features_dim: (int) Number of features extracted.
        This corresponds to the number of unit for the last layer.
    """

    def __init__(self, observation_space: spaces.Box, features_dim: int = 128):
        
        # We assume CxHxW images (channels first)
        # Re-ordering will be done by pre-preprocessing or wrapper
        n_input_channels = observation_space.shape[0]
        self.cnn = nn.Sequential(
            nn.Conv2d(in_channels=n_input_channels,out_channels=32,kernel_size=3),   
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2),
            nn.Conv2d(in_channels=32,out_channels=64,kernel_size=3),   
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2),
            nn.Flatten()
        )

        # Compute shape by doing one forward pass
        with th.no_grad():
            n_flatten = self.cnn(
                th.as_tensor(observation_space.sample()[None]).float()
            ).shape[1]

        self.linear = nn.Sequential(nn.Linear(n_flatten, features_dim), nn.ReLU())

    def forward(self, observations: th.Tensor) -> th.Tensor:
        return self.linear(self.cnn(observations))


class ConvBlock(nn.Module):
    def __init__(self, num_in_channels,num_out_channels):
        super().__init__()
        self.cnn = nn.Sequential(
            nn.Conv2d(in_channels=num_in_channels,out_channels=num_out_channels,kernel_size=3),   
            nn.BatchNorm2d(num_out_channels),
            nn.ReLU(),
            nn.MaxPool2d(2),)

    def forward(self, x):
        return self.cnn(x)