from copy import deepcopy
import gymnasium
import yaml
import flp_environment
from flp_environment.wrappers.discrete_action_space import DiscreteActionSpaceWrapper
from flp_environment.wrappers.observationWrapper import ObservationWrapper
from ray import tune
from ray.rllib.algorithms.algorithm import Algorithm
import ray
import multiprocessing
with open('config.yaml', 'r') as file:
    config = yaml.safe_load(file)
with open('env_config.yaml', 'r') as file:
    env_config = yaml.safe_load(file)
use_case = "Use-case-2"
env_config=env_config[use_case]

ray.init()


def prepare_units(units):
    units_copy = deepcopy(units)
    for i in range(0,len(units_copy)):
        unit = units_copy[i]
        for j in range(0,len(unit)):
            for k in range(0,len(unit[0])):
                unit[j][k]= unit[j][k]+i*3 if unit[j][k] != 0 else 0

    
    return units_copy



def make_env(env_config_1):
    grids = env_config["grids"]
    units = prepare_units(env_config["units"])
    material_flow= env_config["material_flow"]
    fix_cost = env_config["fix_cost"]
    period= env_config["period"]
    interarrival_time= env_config["interarrival_time"]
    initial_positions =env_config["initial_positions"]
    env = gymnasium.make('flp_discrete-v1',desc= grids, units = units, material_flow=material_flow, fix_cost=fix_cost, interarrival_time=interarrival_time,period_length=period,initial_positions=initial_positions) #env = ObservationNormalizerWrapper(env)
    env =DiscreteActionSpaceWrapper(env)
    env = ObservationWrapper(env)
    #env = NormalizeReward(env)
    #env = TimeLimit(env=env, max_episode_steps=len(units)*len(grids))
    return env

from ray.rllib.algorithms.dqn import DQNConfig
from ray.tune.registry import register_env

register_env("flp_d_iscrete-v1", make_env)
def train_rainbow(paras,path,total_timesteps, evaluation_freq, episodes_evaluation):

    config =DQNConfig()
    config.gamma=paras["gamma"]
    config.n_step = paras["n_step"]
    config.noisy=True
    config.dueling =True
    config.double_q=True
    config.sigma0=paras["sigma"]
 
    config.target_network_update_freq=paras["target_network_update_freq"]
  
    
    config.evaluation(evaluation_interval=evaluation_freq, evaluation_duration= episodes_evaluation ,evaluation_config=DQNConfig.overrides(explore=False))
    replay_config = {
                    "type": "MultiAgentPrioritizedReplayBuffer",
                    "capacity": paras["buffer_size"],
                    "prioritized_replay_alpha": 0.2,
                    "prioritized_replay_beta": 0.5,
                }
    
    config.categorical_distribution_temperature = paras["categorical_distribution_temperature"]
    config = config.resources(num_gpus=1)
    
    
    config.exploration_config = {
                "type": "EpsilonGreedy",
                "initial_epsilon":1.0,
                "final_epsilon": paras["final_epsilon"],
                "epsilon_timesteps": paras["epsilon_timesteps"]*total_timesteps,
            }

    config.num_atoms=51
    config.v_max=16
    config.v_min=-1
    #config.evaluation(evaluation_config=DQNConfig.overrides(exploration_config={"explore": False}))
    config.train_batch_size=paras["batch_size"]

    config.hiddens=paras["hiddens"]

    config.lr=paras["learning_rate"]
    config.rollout_fragment_length=4 if 4> paras["n_step"] else paras["n_step"]
    

    config.num_steps_sampled_before_learning_starts=paras["target_network_update_freq"]
    config = config.training(replay_buffer_config=replay_config)

  
    config.environment(env= "flp_d_iscrete-v1" )
    config.framework("torch")
    config.model["fcnet_hiddens"]=[128]
    if "conv_filters" in paras:
        config.model["conv_filters"]= paras["conv_filters"]
    config.model["fcnet_activation"] =paras["fcnet_activation"]

    #print(config.model)

    config.timesteps_per_iteration=1000
    algo = config.build()  # 2. build the algorithm,
    algo.seed = 4
   
    checkpoint_config ={"num_to_keep": 1, 
                        "checkpoint_score_attribute": "episode_reward_mean",
                         "checkpoint_score_order":"max",
                          "checkpoint_frequency":10,
                        
                            }
    number_of_iteration= total_timesteps// config.timesteps_per_iteration
    best_evaluation= -1
    save_path= ""
    for i in range(number_of_iteration):
        
        algo.train()
        if (i % evaluation_freq) == 0:
            evaluation = algo.evaluate()
       
            if (evaluation["evaluation"]["episode_reward_mean"])> best_evaluation:
                best_evaluation= evaluation["evaluation"]["episode_reward_mean"]
                save_path=algo.save(checkpoint_dir=path+"\\run_5")
        print(f"finish {i * 1000} iterations")
        print(f"best evaluation {best_evaluation}")
        
    #analysis = tune.run("DQN", config = config, log_to_file=True,stop={"timesteps_total": total_timesteps},checkpoint_config=checkpoint_config, local_dir= path+"\\model",verbose=0)
    score = algo.evaluate()
    algo.restore(save_path)
    return algo , score["evaluation"]["episode_reward_mean"]
    #print(analysis.trials[0].last_result)
    #print("evaluation: ", analysis.trials[0].last_result["evaluation"] )
    #return Algorithm.from_checkpoint(analysis.get_best_checkpoint(analysis.trials[0],"episode_reward_mean","max")), analysis.trials[0].last_result["evaluation"]["episode_reward_mean"]
    # 4. and evaluate it.

'''paras ={
    "n_step":3,
    "sigma": 0.22882018681838343,
    "buffer_size":15000,
    "categorical_distribution_temperature":1.0,
    "final_epsilon":0.06245981875991809,
    "epsilon_timesteps": 0.35962763464269376,
    "batch_size":512,
    "hiddens":[128,128],
    "learning_rate":0.000891420667839629,
    "target_network_update_freq":15000,
    "num_steps_sampled_before_learning_starts":0,
    "fcnet_activation":"tanh", 
    "gamma": 0.99,
    "conv_filters":[[32,[3,3],2],[64,[3,3],2],[32,[3,3],2]]


}'''

paras = config[use_case]
print(paras)


def train_rainbow_with_analysis(paras,path,total_timesteps, evaluation_freq, episodes_evaluation):

    config =DQNConfig()
    config.gamma=paras["gamma"]
    config.n_step = paras["n_step"]
    config.noisy=True
    config.dueling =True
    config.double_q=True
    config.sigma0=paras["sigma"]

    config.target_network_update_freq=paras["target_network_update_freq"]

    config.evaluation(evaluation_interval=evaluation_freq, evaluation_duration= episodes_evaluation ,evaluation_config=DQNConfig.overrides(explore=False))
    replay_config = {
                    "type": "MultiAgentPrioritizedReplayBuffer",
                    "capacity": paras["buffer_size"],
                    "prioritized_replay_alpha": 0.2,
                    "prioritized_replay_beta": 0.5,
                }
    
    config.categorical_distribution_temperature = paras["categorical_distribution_temperature"]
    config = config.resources(num_gpus=1,num_learner_workers=5)
  
    config.exploration_config = {
                "type": "EpsilonGreedy",
                "initial_epsilon":1.0,
                "final_epsilon": paras["final_epsilon"],
                "epsilon_timesteps": paras["epsilon_timesteps"]*total_timesteps,
            }

    config.num_atoms=51
    config.v_max=16
    config.v_min=-1
    #config.evaluation(evaluation_config=DQNConfig.overrides(exploration_config={"explore": False}))
    config.train_batch_size=paras["batch_size"]

    config.hiddens=paras["hiddens"]

    config.lr=paras["learning_rate"]
    config.rollout_fragment_length=4 if 4> paras["n_step"] else paras["n_step"]
    config.seed= 2

    config.num_steps_sampled_before_learning_starts=paras["target_network_update_freq"]
    config = config.training(replay_buffer_config=replay_config)

  
    config.environment(env= "flp_d_iscrete-v1" )
    config.framework("torch")
    config.model["fcnet_hiddens"]=[128]
    if "conv_filters" in paras:
        config.model["conv_filters"]= paras["conv_filters"]
    config.model["fcnet_activation"] =paras["fcnet_activation"]
    
    #print(config.model)

    config.timesteps_per_iteration=1000
    #algo = config.build()  # 2. build the algorithm,
  
    checkpoint_config ={"num_to_keep": 1, 
                        "checkpoint_score_attribute": "episode_reward_mean",
                         "checkpoint_score_order":"max",
                          "checkpoint_frequency":1,
                        
                            }
    number_of_iteration= total_timesteps// config.timesteps_per_iteration
    best_evaluation= -1
    save_path= ""
    
        
    analysis = tune.run("DQN", config = config, log_to_file=True,stop={"timesteps_total": total_timesteps},checkpoint_config=checkpoint_config, local_dir= path+"\\run2",verbose=1)
   
    print("evaluation: ", analysis.trials[0].last_result )
    
    return Algorithm.from_checkpoint(analysis.get_best_checkpoint(analysis.trials[0],"episode_reward_mean","max"))#, analysis.trials[0].last_result["evaluation"]["episode_reward_mean"]
    # 4. and evaluate it.



def test(number_of_test_episodes, algo,evaluation_env):
    
    for _ in range(0,number_of_test_episodes):
        obs, info = evaluation_env.reset()
        episode_reward = 0
        terminated = truncated = False
        while not terminated and not truncated:
            action = algo.compute_single_action(obs)
        
            obs, reward, terminated, truncated, info = evaluation_env.step(action)
            
            episode_reward += reward
        print(episode_reward)
        print(evaluation_env.grid)

if __name__ == "__main__":
    algo,score = train_rainbow(paras,"C:\\Users\\innoc\\OneDrive\\Desktop\\Master_thesis\\RLlib\\runs",paras["training_steps"],1,10)
    #algo = train_rainbow_with_analysis(paras,"C:\\Users\\innoc\\OneDrive\\Desktop\\Master_thesis\\RLlib\\test",1000000,1,10)
    #print("last score: ", score)
    evaluation_env = make_env(None)
    test(10,algo,evaluation_env)