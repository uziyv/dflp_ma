import argparse
import glob
import importlib
import os
import pickle as pkl
import time
import warnings
from collections import OrderedDict
from pathlib import Path
from pprint import pprint
from typing import Any, Callable, Dict, List, Optional, Tuple, Union



import main
from main import make_env

import numpy as np
import optuna
import torch as th
import yaml

from optuna.pruners import BasePruner, MedianPruner, NopPruner, SuccessiveHalvingPruner
from optuna.samplers import BaseSampler, RandomSampler, TPESampler
from optuna.study import MaxTrialsCallback
from optuna.trial import TrialState
from optuna.visualization import plot_optimization_history, plot_param_importances
from flp_environment.wrappers.discrete_action_space import DiscreteActionSpaceWrapper
from flp_environment.wrappers.observationWrapper import ObservationWrapper
from main import train_rainbow

# For using HER with GoalEnv

# For custom activation fn
from torch import nn as nn


from HyperParameters import HYPERPARAMS_SAMPLER


def get_latest_run_id(log_path: str) -> int:
    """
    Returns the latest run number for the given log name and log path,
    by finding the greatest number in the directories.

    :param log_path: path to log folder
    :param env_name:
    :return: latest run number
    """
    max_run_id = 0
    env_name="dflp"
    for path in glob.glob(os.path.join(log_path, env_name + "_[0-9]*")):
        run_id = path.split("_")[-1]
        path_without_run_id = path[: -len(run_id) - 1]
        if path_without_run_id.endswith(env_name) and run_id.isdigit() and int(run_id) > max_run_id:
            max_run_id = int(run_id)
    return max_run_id

class ExperimentManager:
    """
    Experiment manager: read the hyperparameters,
    preprocess them, create the environment and the RL model.

    Please take a look at `train.py` to have the details for each argument.
    """

    def __init__(
        self,
        args: argparse.Namespace,
        algo: str,
        env_id: str,
        log_folder: str,
        tensorboard_log: str = "",
        n_timesteps: int = 0,
        eval_freq: int = 10000,
        n_eval_episodes: int = 5,
        save_freq: int = -1,
        hyperparams: Optional[Dict[str, Any]] = None,
        env_kwargs: Optional[Dict[str, Any]] = None,
        eval_env_kwargs: Optional[Dict[str, Any]] = None,
        trained_agent: str = "",
        optimize_hyperparameters: bool = False,
        storage: Optional[str] = None,
        study_name: Optional[str] = None,
        n_trials: int = 1,
        max_total_trials: Optional[int] = None,
        n_jobs: int = 1,
        sampler: str = "tpe",
        pruner: str = "median",
        optimization_log_path: Optional[str] = None,
        n_startup_trials: int = 0,
        n_evaluations: int = 1,
        truncate_last_trajectory: bool = False,
        uuid_str: str = "",
        seed: int = 0,
        log_interval: int = 0,
        save_replay_buffer: bool = False,
        verbose: int = 1,
        vec_env_type: str = "dummy",
        n_eval_envs: int = 1,
        no_optim_plots: bool = False,
        device: Union[th.device, str] = "auto",
        config: Optional[str] = None,
        show_progress: bool = False,
    ):
        super().__init__()
        self.algo = algo
       
        # Custom params
        self.custom_hyperparams = hyperparams
        if (Path(__file__).parent / "hyperparams").is_dir():
            # Package version
            default_path = Path(__file__).parent
        else:
            # Take the root folder
            default_path = Path(__file__).parent.parent

        self.config = config or str(default_path / f"hyperparams/{self.algo}.yml")
        self.env_kwargs: Dict[str, Any] = env_kwargs or {}
        self.n_timesteps = n_timesteps
        self.normalize = False
        self.normalize_kwargs: Dict[str, Any] = {}
        self.env_wrapper: Optional[Callable] = None
        self.frame_stack = None
        self.seed = seed
        self.optimization_log_path = optimization_log_path

       
        self.vec_env_wrapper: Optional[Callable] = None

        self.vec_env_kwargs: Dict[str, Any] = {}
        # self.vec_env_kwargs = {} if vec_env_type == "dummy" else {"start_method": "fork"}

        # Callbacks
        self.specified_callbacks: List = []
       
        # Use env-kwargs if eval_env_kwargs was not specified
        self.eval_env_kwargs: Dict[str, Any] = eval_env_kwargs or self.env_kwargs
        self.save_freq = save_freq
        self.eval_freq = eval_freq
        self.n_eval_episodes = n_eval_episodes
        self.n_eval_envs = n_eval_envs
        self.env_name="flp_discrete-v1"
        self.n_envs = 8  # it will be updated when reading hyperparams
        self.n_actions = 0  # For DDPG/TD3 action noise objects
        self._hyperparams: Dict[str, Any] = {}
        self.monitor_kwargs: Dict[str, Any] = {}

        self.trained_agent = trained_agent
        self.continue_training = trained_agent.endswith(".zip") and os.path.isfile(trained_agent)
        self.truncate_last_trajectory = truncate_last_trajectory

        #self._is_atari = self.is_atari(env_id)
        # Hyperparameter optimization config
        self.optimize_hyperparameters = optimize_hyperparameters
        self.storage = storage
        self.study_name = study_name
        self.no_optim_plots = no_optim_plots
        # maximum number of trials for finding the best hyperparams
        self.n_trials = n_trials
        self.max_total_trials = max_total_trials
        # number of parallel jobs when doing hyperparameter search
        self.n_jobs = n_jobs
        self.sampler = sampler
        self.pruner = pruner
        self.n_startup_trials = n_startup_trials
        self.n_evaluations = n_evaluations
        #self.deterministic_eval = not (self.is_atari(env_id) or self.is_minigrid(env_id))
        self.device = device

        # Logging
        self.log_folder = log_folder
        self.tensorboard_log = None if tensorboard_log == "" else os.path.join(tensorboard_log, self.env_name)
        self.verbose = verbose
        self.args = args
        self.log_interval = log_interval
        self.save_replay_buffer = save_replay_buffer
        self.show_progress = show_progress

        self.log_path = f"{log_folder}/{self.algo}/"
        self.save_path = os.path.join(
            self.log_path, f"{self.env_name}_{get_latest_run_id(self.log_path) + 1}{uuid_str}"
        )
        self.params_path = f"{self.save_path}/{self.env_name}"

    

    
    def read_hyperparameters(self) -> Tuple[Dict[str, Any], Dict[str, Any]]:
        print(f"Loading hyperparameters from: {self.config}")

        if self.config.endswith(".yml") or self.config.endswith(".yaml"):
            # Load hyperparameters from yaml file
            with open(self.config) as f:
                hyperparams_dict = yaml.safe_load(f)
        elif self.config.endswith(".py"):
            global_variables: Dict = {}
            # Load hyperparameters from python file
            exec(Path(self.config).read_text(), global_variables)
            hyperparams_dict = global_variables["hyperparams"]
        else:
            # Load hyperparameters from python package
            hyperparams_dict = importlib.import_module(self.config).hyperparams
            # raise ValueError(f"Unsupported config file format: {self.config}")

        if self.env_name.gym_id in list(hyperparams_dict.keys()):
            hyperparams = hyperparams_dict[self.env_name.gym_id]
        elif self._is_atari:
            hyperparams = hyperparams_dict["atari"]
        else:
            raise ValueError(f"Hyperparameters not found for {self.algo}-{self.env_name.gym_id} in {self.config}")

        if self.custom_hyperparams is not None:
            # Overwrite hyperparams if needed
            hyperparams.update(self.custom_hyperparams)
        # Sort hyperparams that will be saved
        saved_hyperparams = OrderedDict([(key, hyperparams[key]) for key in sorted(hyperparams.keys())])

        # Always print used hyperparameters
        print("Default hyperparameters for environment (ones being tuned will be overridden):")
        pprint(saved_hyperparams)

        return hyperparams, saved_hyperparams

   

    def _preprocess_normalization(self, hyperparams: Dict[str, Any]) -> Dict[str, Any]:
        if "normalize" in hyperparams.keys():
            self.normalize = hyperparams["normalize"]

            # Special case, instead of both normalizing
            # both observation and reward, we can normalize one of the two.
            # in that case `hyperparams["normalize"]` is a string
            # that can be evaluated as python,
            # ex: "dict(norm_obs=False, norm_reward=True)"
            if isinstance(self.normalize, str):
                self.normalize_kwargs = eval(self.normalize)
                self.normalize = True

            if isinstance(self.normalize, dict):
                self.normalize_kwargs = self.normalize
                self.normalize = True

            # Use the same discount factor as for the algorithm
            if "gamma" in hyperparams:
                self.normalize_kwargs["gamma"] = hyperparams["gamma"]

            del hyperparams["normalize"]
        return hyperparams

    
   

    def create_log_folder(self):
        os.makedirs(self.params_path, exist_ok=True)

  

 
    @staticmethod
    def is_atari(env_id: str) -> bool:
        return "AtariEnv" in ExperimentManager.entry_point(env_id)

    @staticmethod
    def is_minigrid(env_id: str) -> bool:
        return "minigrid" in ExperimentManager.entry_point(env_id)

    @staticmethod
    def is_bullet(env_id: str) -> bool:
        return "pybullet_envs" in ExperimentManager.entry_point(env_id)

    @staticmethod
    def is_robotics_env(env_id: str) -> bool:
        return "gym.envs.robotics" in ExperimentManager.entry_point(
            env_id
        ) or "panda_gym.envs" in ExperimentManager.entry_point(env_id)

    @staticmethod
    def is_panda_gym(env_id: str) -> bool:
        return "panda_gym.envs" in ExperimentManager.entry_point(env_id)



    def create_envs(self, n_envs: int, eval_env: bool = False, no_log: bool = False) :
        mdp = make_env(None)
      
        return mdp

   

    def _create_sampler(self, sampler_method: str) -> BaseSampler:
        # n_warmup_steps: Disable pruner until the trial reaches the given number of steps.
        if sampler_method == "random":
            sampler: BaseSampler = RandomSampler(seed=self.seed)
        elif sampler_method == "tpe":
            sampler = TPESampler(n_startup_trials=self.n_startup_trials, seed=self.seed, multivariate=True)
        elif sampler_method == "skopt":
            from optuna.integration.skopt import SkoptSampler

            # cf https://scikit-optimize.github.io/#skopt.Optimizer
            # GP: gaussian process
            # Gradient boosted regression: GBRT
            sampler = SkoptSampler(skopt_kwargs={"base_estimator": "GP", "acq_func": "gp_hedge"})
        else:
            raise ValueError(f"Unknown sampler: {sampler_method}")
        return sampler

    def _create_pruner(self, pruner_method: str) -> BasePruner:
        if pruner_method == "halving":
            pruner: BasePruner = SuccessiveHalvingPruner(min_resource=1, reduction_factor=4, min_early_stopping_rate=0)
        elif pruner_method == "median":
            pruner = MedianPruner(n_startup_trials=self.n_startup_trials, n_warmup_steps=self.n_evaluations // 3)
        elif pruner_method == "none":
            # Do not prune
            pruner = NopPruner()
        else:
            raise ValueError(f"Unknown pruner: {pruner_method}")
        return pruner

    def objective(self, trial: optuna.Trial) -> float:
        kwargs = self._hyperparams.copy()

        n_envs = 1 if self.algo == "ars" else self.n_envs

        additional_args = { }
        # Pass n_actions to initialize DDPG/TD3 noise sampler
        # Sample candidate hyperparameters
        sampled_hyperparams = HYPERPARAMS_SAMPLER[self.algo](trial, self.n_actions, n_envs, additional_args)
        
        kwargs.update(sampled_hyperparams)

        env = self.create_envs(n_envs, no_log=True)

        # By default, do not activate verbose output to keep
        # stdout clean with only the trial's results
        trial_verbosity = 0
        # Activate verbose mode for the trial in debug mode
        # See PR #214
        if self.verbose >= 2:
            trial_verbosity = self.verbose
        path = None
        if self.optimization_log_path is not None:
            path = os.path.join(self.optimization_log_path, f"trial_{trial.number!s}")
            os.makedirs(path, exist_ok=True)
            
      

        eval_env = self.create_envs(n_envs=self.n_eval_envs, eval_env=True)

        optuna_eval_freq = int(self.n_timesteps / self.n_evaluations)
        # Account for parallel envs
        optuna_eval_freq = max(optuna_eval_freq // self.n_envs, 1)
        # Use non-deterministic eval for Atari
       

       


        f = open(path+"/hyp.txt", "w")
        f.write(str(sampled_hyperparams))
        f.close()

        learn_kwargs = {}
        # Special case for ARS
        try:
            agent, score = train_rainbow(sampled_hyperparams, path,self.n_timesteps, self.eval_freq,self.n_eval_episodes)
            # Free memory
         
           
            test(self.n_eval_episodes,agent,eval_env,path)

        except (AssertionError, ValueError) as e:
            # Sometimes, random hyperparams can generate NaN
            # Free memory
       
            # Prune hyperparams that generate NaNs
            print(e)
            print("============")
            print("Sampled hyperparams:")
            pprint(sampled_hyperparams)
            raise optuna.exceptions.TrialPruned() from e


        del env, eval_env
        del agent


        return score

    def hyperparameters_optimization(self) -> None:
        if self.verbose > 0:
            print("Optimizing hyperparameters")

        if self.storage is not None and self.study_name is None:
            warnings.warn(
                f"You passed a remote storage: {self.storage} but no `--study-name`."
                "The study name will be generated by Optuna, make sure to re-use the same study name "
                "when you want to do distributed hyperparameter optimization."
            )

        if self.tensorboard_log is not None:
            warnings.warn("Tensorboard log is deactivated when running hyperparameter optimization")
            self.tensorboard_log = None

        # TODO: eval each hyperparams several times to account for noisy evaluation
        sampler = self._create_sampler(self.sampler)
        pruner = self._create_pruner(self.pruner)

        if self.verbose > 0:
            print(f"Sampler: {self.sampler} - Pruner: {self.pruner}")

        study = optuna.create_study(
            sampler=sampler,
            pruner=pruner,
            storage=self.storage,
            study_name=self.study_name,
            load_if_exists=True,
            direction="maximize",
        )

        try:
            if self.max_total_trials is not None:
                # Note: we count already running trials here otherwise we get
                #  (max_total_trials + number of workers) trials in total.
                counted_states = [
                    TrialState.COMPLETE,
                    TrialState.RUNNING,
                    TrialState.PRUNED,
                ]
                completed_trials = len(study.get_trials(states=counted_states))
                if completed_trials < self.max_total_trials:
                    study.optimize(
                        self.objective,
                        n_jobs=self.n_jobs,
                        callbacks=[
                            MaxTrialsCallback(
                                self.max_total_trials,
                                states=counted_states,
                            )
                        ],
                    )
            else:
                study.optimize(self.objective, n_jobs=self.n_jobs, n_trials=self.n_trials)
        except KeyboardInterrupt:
            pass

        print("Number of finished trials: ", len(study.trials))

        print("Best trial:")
        trial = study.best_trial

        print("Value: ", trial.value)

        print("Params: ")
        for key, value in trial.params.items():
            print(f"    {key}: {value}")

        report_name = (
            f"report_{self.env_name}_{self.n_trials}-trials-{self.n_timesteps}"
            f"-{self.sampler}-{self.pruner}_{int(time.time())}"
        )

        log_path = os.path.join(self.log_folder, self.algo, report_name)

        if self.verbose:
            print(f"Writing report to {log_path}")

        # Write report
        os.makedirs(os.path.dirname(log_path), exist_ok=True)
        study.trials_dataframe().to_csv(f"{log_path}.csv")

        # Save python object to inspect/re-use it later
        with open(f"{log_path}.pkl", "wb+") as f:
            pkl.dump(study, f)

        # Skip plots
        if self.no_optim_plots:
            return

        # Plot optimization result
        try:
            fig1 = plot_optimization_history(study)
            fig2 = plot_param_importances(study)

            fig1.show()
            fig2.show()
        except (ValueError, ImportError, RuntimeError):
            pass

algo= "rainbow"

def test(number_of_test_iterations, agent, env,path):
    f = open(path+"/best_Model_performance.txt", "w")
           

    mean_result= 0
    for _ in range(number_of_test_iterations):

        terminal = False
        obs, info = env.reset()

        sum_rewards = 0
        
        while not terminal:
            action = agent.compute_single_action(obs)
        
            obs, reward, terminal, truncated, info = env.step(action)
            
            sum_rewards += reward
            

            f.write(f"action: {action}\n")
            f.write(f"reward: {reward}\n")
            
        

        f.write(f"end reward for following:  {sum_rewards}\n")
      
        f.write(str(env.env.grid))
        mean_result += sum_rewards

    mean_result /= number_of_test_iterations
    f.write(f"\n mean reward: {mean_result}\n")
    f.close()
    return mean_result

exp_manager = ExperimentManager(
        {},
        algo,
        "flp_discrete-v1",
        "log/",
        "tensorboard/",
        500000,
        50,
        10,
        -1,
        None,
        dict(wrappers=[DiscreteActionSpaceWrapper, ObservationWrapper]),
        dict(wrappers=[DiscreteActionSpaceWrapper, ObservationWrapper]),
        "best",
        True,
        None,
        f"{algo}_HO",
        50,
        None,
        2,
        "tpe",
        "median",
        f"HO/{algo}/",
        n_startup_trials=10,
        n_evaluations=1000,
        show_progress=True,
        no_optim_plots=False
    
    )
exp_manager.hyperparameters_optimization()