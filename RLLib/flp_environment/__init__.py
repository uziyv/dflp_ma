import logging
from gymnasium.envs.registration import register

logger = logging.getLogger(__name__)


register(  id='flp_discrete-v1',
          entry_point='flp_environment.envs:FLP_Environment_discrete') 

