from copy import deepcopy
import gymnasium
import yaml
import flp_environment.envs.environment_discrete as flp
from flp_environment.envs.environment_discrete import FLP_Environment_discrete
from flp_environment.wrappers.discrete_action_space import DiscreteActionSpaceWrapper
import unittest
import numpy.testing as np_testing
with open('env_config.yaml', 'r') as file:
        env_config = yaml.safe_load(file)

use_case = "Use-case-2"
env_config = env_config[use_case]
def prepare_units(units):
    units_copy = deepcopy(units)
    for i in range(0, len(units_copy)):
        unit = units_copy[i]
        for j in range(0, len(unit)):
            for k in range(0, len(unit[0])):
                unit[j][k] = unit[j][k]+i*3 if unit[j][k] != 0 else 0

    return units_copy


def make_env():
    grids = env_config["grids"]
    units = prepare_units(env_config["units"])
    material_flow = env_config["material_flow"]
    fix_cost = env_config["fix_cost"]
    period = env_config["period"]
    interarrival_time = env_config["interarrival_time"]
    initial_positions = env_config["initial_positions"]
    env = FLP_Environment_discrete( desc=grids, units=units, material_flow=material_flow, fix_cost=fix_cost,
                   initial_positions=initial_positions, period_length=period, interarrival_time=interarrival_time)

    return env
def make_discrete_env():
    grids = env_config["grids"]
    units = prepare_units(env_config["units"])
    material_flow = env_config["material_flow"]
    fix_cost = env_config["fix_cost"]
    period = env_config["period"]
    interarrival_time = env_config["interarrival_time"]
    initial_positions = env_config["initial_positions"]
    env = DiscreteActionSpaceWrapper(FLP_Environment_discrete( desc=grids, units=units, material_flow=material_flow, fix_cost=fix_cost,
                   initial_positions=initial_positions, period_length=period, interarrival_time=interarrival_time))

    return env



class TestEnvironemnt(unittest.TestCase):

    def setUp(self) -> None:
        self.env = make_env()
        self.env.reset()
    def tearDown(self) -> None:
        self.env.reset()
    def test_reset_method(self):
        self.env.reset()
        self.assertEqual(self.env.time_step, 0)
        self.assertEqual(self.env.step_in_one_period, 0)
        self.assertEqual(self.env.index_of_unit_to_set, 3)
        for i in range(0, self.env.n_units):
            np_testing.assert_allclose(self.env.units_in_last_step[i], self.env.units[i])
        for i in range(0, self.env.n_units):
            self.assertEqual(self.env.distances[i], -1)
    def test_reward_function(self):
        action=[5,0,3]
        state,reward, terminal, _, _ = self.env.step(action)
        expected_reward= self.env.nS/2 - 4 - 4*0.8
        expected_reward_norm = expected_reward/( self.env.nS/2)
        self.assertEqual(reward, expected_reward_norm)
        action =[4,7,2]
        state,reward, terminal, _, _ = self.env.step(action)
        expected_reward= self.env.nS/2 - 7 - 7*0.8
        expected_reward_norm = expected_reward/( self.env.nS/2)
        self.assertEqual(reward, expected_reward_norm)
        action =[8,4,1]
        state,reward, terminal, _, _ = self.env.step(action)
        expected_reward= self.env.nS/2 - 5 - 5*0.8
        expected_reward_norm = expected_reward/( self.env.nS/2)
        self.assertEqual(reward, expected_reward_norm)
        action =[6,10,2]

        state,reward, terminal, _, _ = self.env.step(action)
        expected_reward= self.env.nS/2 - 5 - 5*0.8-15-15*0.8
        expected_reward+=self.env.nS/2 -14 -14*0.08
        expected_reward+=self.env.nS/2 -5 - 5*0.08
        expected_reward+=self.env.nS/2 -5 -5*0.08
        expected_reward+=self.env.nS/2 -7 -7*0.08 -25 -25*0.08

        expected_reward_norm = expected_reward/( self.env.nS/2)

        self.assertAlmostEqual(reward, expected_reward_norm)
        
        self.test_reset_method()


    def test_punishment_for_changing_position(self):
        action=[5,0,3]
        state,reward, terminal, _, _ = self.env.step(action)
        expected_reward= self.env.nS/2 - 4 - 4*0.8
        expected_reward_norm = expected_reward/( self.env.nS/2)
        self.assertEqual(reward, expected_reward_norm)
        action =[4,7,2]
        state,reward, terminal, _, _ = self.env.step(action)
        expected_reward= self.env.nS/2 - 7 - 7*0.8
        expected_reward_norm = expected_reward/( self.env.nS/2)
        self.assertEqual(reward, expected_reward_norm)
        action =[8,4,1]
        state,reward, terminal, _, _ = self.env.step(action)
        expected_reward= self.env.nS/2 - 5 - 5*0.8
        expected_reward_norm = expected_reward/( self.env.nS/2)
        self.assertEqual(reward, expected_reward_norm)
        action =[6,10,2]

        state,reward, terminal, _, _ = self.env.step(action)
        expected_reward= self.env.nS/2 - 5 - 5*0.8-15-15*0.8
        expected_reward+=self.env.nS/2 -14 -14*0.08
        expected_reward+=self.env.nS/2 -5 - 5*0.08
        expected_reward+=self.env.nS/2 -5 -5*0.08
        expected_reward+=self.env.nS/2 -7 -7*0.08 -25 -25*0.08

        expected_reward_norm = expected_reward/( self.env.nS/2)

        self.assertAlmostEqual(reward, expected_reward_norm)

        # changing rotataion
        action =[6,10,1]
        state,reward, terminal, _, _ = self.env.step(action)
        expected_reward= self.env.nS/2 -17 - 17*0.8 -1
        expected_reward_norm = expected_reward/( self.env.nS/2)
        self.assertAlmostEqual(reward, expected_reward_norm)

        # changing position
        action =[0,8,0]
        state,reward, terminal, _, _ = self.env.step(action)
       
        expected_reward= self.env.nS/2 - 12- 12*0.8-1
        expected_reward_norm = expected_reward/( self.env.nS/2)
        self.assertEqual(reward, expected_reward_norm)
       
        # changing both 
        action =[5,6,1]
        state,reward, terminal, _, _ = self.env.step(action)
     
        expected_reward= self.env.nS/2 - 12- 12*0.8-1
        expected_reward_norm = expected_reward/( self.env.nS/2)
        self.assertEqual(reward, expected_reward_norm)



        
        self.test_reset_method()
    def test_for_setting_unit_on_invalid_position(self):
        action=[0,0,0]
        state,reward, terminal, _, _ = self.env.step(action)
        
        expected_reward_norm = -1
        self.assertEqual(reward, expected_reward_norm)
    def test_cutting_flow(self):
        action=[6,0,3]
        state,reward, terminal, _, _ = self.env.step(action)
        expected_reward= self.env.nS/2 - 5 - 5*0.8
        expected_reward_norm = expected_reward/( self.env.nS/2)
        self.assertEqual(reward, expected_reward_norm)

        self.assertEqual(self.env.distances[3], 5)

        action =[3,1,2]
        state,reward, terminal, _, _ = self.env.step(action)
        
        expected_reward= self.env.nS/2 - 1 - 1*0.8-2-2*0.8
        expected_reward_norm = expected_reward/( self.env.nS/2)
        self.assertEqual(reward, expected_reward_norm)

        self.assertEqual(self.env.distances[3], 7)



    def test_blocking_main_flow(self):
        action=[1,0,3]
        state,reward, terminal, _, _ = self.env.step(action)
        expected_reward= self.env.nS/2 - 0 - 0*0.8
        expected_reward_norm = expected_reward/( self.env.nS/2)
        self.assertEqual(reward, expected_reward_norm)
        action =[4,7,2]
        state,reward, terminal, _, _ = self.env.step(action)
        expected_reward= -1
       
        self.assertEqual(reward, expected_reward)
        
        
    def test_blocking_other_flow(self):
        action=[5,0,3]
        state,reward, terminal, _, _ = self.env.step(action)
        expected_reward= self.env.nS/2 - 4 - 4*0.8
        expected_reward_norm = expected_reward/( self.env.nS/2)
        self.assertEqual(reward, expected_reward_norm)

        action =[4,7,2]
        state,reward, terminal, _, _ = self.env.step(action)
        expected_reward= self.env.nS/2 - 7 - 7*0.8
        expected_reward_norm = expected_reward/( self.env.nS/2)
        self.assertEqual(reward, expected_reward_norm)

        action =[7,4,2]
        state,reward, terminal, _, _ = self.env.step(action)
        expected_reward= self.env.nS/2 - 3 - 3*0.8
        expected_reward_norm = expected_reward/( self.env.nS/2)
        self.assertEqual(reward, expected_reward_norm)


        action =[7,8,0]
        state,reward, terminal, _, _ = self.env.step(action)
        expected_reward_norm = -1
        self.assertEqual(reward, expected_reward_norm)
        print(self.env.grid)
        self.test_reset_method()




def test(env,steps):

    return_all = 0 
    for step in steps:
        state, reward, terminal ,_,_ =env.step(step)
        return_all += reward
    print("return: ", return_all)

  
    
    '''
    state, reward, terminal ,_,_ =env.step(760)
    state, reward, terminal ,_,_ =env.step(435)
  
    state, reward, terminal ,_,_ =env.step(396)
    state, reward, terminal ,_,_ =env.step(370)
    state, reward, terminal ,_,_ =env.step(359)
    state, reward, terminal ,_,_ =env.step(95)
    state, reward, terminal ,_,_ =env.step(284)
    state, reward, terminal ,_,_ =env.step(339)
    state, reward, terminal ,_,_ =env.step(373)
    state, reward, terminal ,_,_ =env.step(737)
    state, reward, terminal ,_,_ =env.step(37)
    state, reward, terminal ,_,_ =env.step(283)
    state, reward, terminal ,_,_ =env.step(7)
    state, reward, terminal ,_,_ =env.step(244)
    state, reward, terminal ,_,_ =env.step(425)
    print(reward)'''


steps= [248,534,60,337,336,60,353,364,369,88,362,394,340,118,308,337]


def test_2(env):
  
    state, reward, terminal ,_,_ =env.step(291)
    print(reward)
   
    state, reward, terminal ,_,_ =env.step(11)
    print(reward)
  
    state, reward, terminal ,_,_ =env.step(32)
    print(reward)
    state, reward, terminal ,_,_ =env.step(366)
    print(reward)
    state, reward, terminal ,_,_ =env.step(291)
    print(reward)
    state, reward, terminal ,_,_ =env.step(32)
    state, reward, terminal ,_,_ =env.step(245)
    
    
    '''
    state, reward, terminal ,_,_ =env.step(339)
    state, reward, terminal ,_,_ =env.step(373)
    state, reward, terminal ,_,_ =env.step(737)
    state, reward, terminal ,_,_ =env.step(37)
    state, reward, terminal ,_,_ =env.step(283)
    state, reward, terminal ,_,_ =env.step(7)
    state, reward, terminal ,_,_ =env.step(244)
    state, reward, terminal ,_,_ =env.step(425)'''
    print(reward)

steps_27_4= [531,362,380,39,32,375,758,298,32,291,59,307,282,60,249,423]


if __name__ == "__main__":
    #unittest.main()
    
    #env = make_discrete_env()
    #env.reset()
    #steps_28 = [248,534,60,337,336,60,353,364,369,88,362,394,340,118,308,377]
    #steps_27_4= [531,362,380,39,32,375,758,298,32,291,59,307,282,60,249,423]
    env = make_env()
    env.reset()
    steps = [[5,0,2],[5,7,2],[7,8,3],[7,3,0], [2,2,3],[7,3,3],[5,6,0],[7,7,2], [2,2,0],[7,3,0],[5,6,0],[7,7,0], [5,0,2],[5,7,0],[7,8,1],[7,3,1]]
    test(env, steps)
    print(env.grid)
  
    #
    #test_2(env)
    #print(env.grid)


 


