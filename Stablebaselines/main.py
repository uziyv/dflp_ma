from rl_zoo3 import linear_schedule
from stable_baselines3.common.callbacks import EvalCallback
import logging
from copy import deepcopy
import random
import nevergrad
import numpy as np
import gymnasium as gym
import stable_baselines3
import torch
import flp_environment
from stable_baselines3 import DQN, PPO, A2C, SAC, TD3, A2C, DDPG
from stable_baselines3.common.noise import NormalActionNoise, OrnsteinUhlenbeckActionNoise
from gymnasium.envs.registration import register
from stable_baselines3.common.evaluation import evaluate_policy
import glob
import os
import shlex
import subprocess
import wandb
from wandb.integration.sb3 import WandbCallback
from flp_environment.wrappers.observationWrapper import ObservationWrapper
from utils.best_model_saver import SaveOnBestTrainingRewardCallback
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common import results_plotter
import matplotlib.pyplot as plt
from stable_baselines3.common.results_plotter import load_results, ts2xy
import yaml
from flp_environment.wrappers.continous_action_space import ContinousActionSpaceWrapper
from flp_environment.wrappers.discrete_action_space import DiscreteActionSpaceWrapper
from flp_environment.wrappers.multidiscrete_action_space import MultiDiscreteActionSpaceWrapper
from flp_environment.wrappers.observationNormalizer import ObservationNormalizerWrapper
from gymnasium.wrappers.normalize import NormalizeObservation, NormalizeReward
from gymnasium.wrappers.time_limit import TimeLimit
import torch as th
from utils.custom_network import CustomCNN
from utils.featureExtractor import FeatureExtractor
import logging
from stable_baselines3.common.env_util import make_vec_env


with open('config.yaml', 'r') as file:
    config = yaml.safe_load(file)
with open('env_config.yaml', 'r') as file:
    env_config = yaml.safe_load(file)
use_case = "Use-case-2"
env_config = env_config[use_case]
agent_seed = config["general"]["agent_seed"] if "agent_seed" in config["general"] else None
should_wandb = config["general"]["wandb"]
selected_agent = config["general"]["selected_agent"]
tune_hyper = config["general"]["tune_hyper"]
config = config[use_case]




def prepare_units(units):
    '''
    Prepares the given units and encodes them. The units are encoded in order. The first unit is encoded with 0,1,2 and the second with 3,4,5, ...etc
    '''
    units_copy = deepcopy(units)
    for i in range(0, len(units_copy)):
        unit = units_copy[i]
        for j in range(0, len(unit)):
            for k in range(0, len(unit[0])):
                unit[j][k] = unit[j][k]+i*3 if unit[j][k] != 0 else 0

    return units_copy


def make_env(wrappers):
    '''
    creates the environement 
    '''
    grids = env_config["grids"]
    units = prepare_units(env_config["units"])
    material_flow = env_config["material_flow"]
    fix_cost = env_config["fix_cost"]
    period = env_config["period"]
    interarrival_time = env_config["interarrival_time"]
    initial_positions = env_config["initial_positions"]
    env = gym.make('flp_discrete-v1', desc=grids, units=units, material_flow=material_flow, fix_cost=fix_cost,
                   initial_positions=initial_positions, period_length=period, interarrival_time=interarrival_time)
    
    for wrapper in wrappers: 
        env= wrapper(env)

    return env

def make_vectorized_environment(wrappers):

    env = make_vec_env(make_env, n_envs=8, env_kwargs=dict(wrappers = wrappers))

    return env

env_dis = make_env([DiscreteActionSpaceWrapper,ObservationWrapper])
env_cont= make_env([ContinousActionSpaceWrapper,ObservationWrapper])
env_mul_dis =  make_env([DiscreteActionSpaceWrapper,ObservationWrapper])

vec_env_dis= make_vectorized_environment([DiscreteActionSpaceWrapper,ObservationWrapper])
vec_env_cont= make_vectorized_environment([ContinousActionSpaceWrapper,ObservationWrapper])
vec_env_mul_dis =  make_vectorized_environment([DiscreteActionSpaceWrapper,ObservationWrapper])

log_dir = "tmp_1/"
os.makedirs(log_dir, exist_ok=True)
env_dis = Monitor(env_dis, log_dir)
env_cont = Monitor(env_cont, log_dir)
env_mul_dis = Monitor(env_mul_dis, log_dir)

logging.basicConfig(filename= f"{log_dir}/std.log",
                    format='%(asctime)s %(message)s',
                    filemode='a')
logger=logging.getLogger() 
logger.setLevel(logging.DEBUG) 
N_STEPS = 10000
N_TRIALS = 50
N_JOBS = 1

logger.info("\n \n \n ***************************************** new Hyperparameter tuning ******************************** \n \n \n")

def get_ppo_agent(config, verbose=1):
    print("init ppo agent")
    print(config)
    ppo = PPO("MlpPolicy", vec_env_mul_dis, verbose=verbose, seed=agent_seed,
              tensorboard_log="./ppo_dflp_discrete_tensorboard/", device="cpu")
    learning_rate = linear_schedule(config["learning_rate"]) if "learning_rate" in config else ppo.learning_rate
    batch_size = config["batch_size"] if "batch_size" in config else ppo.batch_size
    n_steps = config["n_steps"]if "n_steps" in config else ppo.n_steps
    ent_coef = config["ent_coef"]if "ent_coef" in config else ppo.ent_coef
    gamma = config["gamma"]if "gamma" in config else ppo.gamma
    gae_lambda = config["gae_lambda"]if "gae_lambda" in config else ppo.gae_lambda
    n_epochs = config["n_epochs"]if "n_epochs" in config else ppo.n_epochs
    clip_range = config["clip_range"]if "clip_range" in config else ppo.n_epochs
    max_grad_norm = config["max_grad_norm"] if "max_grad_norm" in config else ppo.max_grad_norm
    vf_coef = config["vf_coef"] if "vf_coef" in config else ppo.vf_coef
    pi_network = config["pi_network"]if "pi_network" in config else [64, 64]
    value_network = config["value_network"]if "value_network" in config else [64, 64]
    ppo = PPO("MlpPolicy", vec_env_mul_dis, verbose=verbose, seed=agent_seed, learning_rate=learning_rate, batch_size=batch_size,max_grad_norm=max_grad_norm, vf_coef=vf_coef,
              n_steps=n_steps, ent_coef=ent_coef, gamma=gamma, gae_lambda=gae_lambda, n_epochs=n_epochs, clip_range=clip_range,
              tensorboard_log="./ppo_dflp_discrete_tensorboard/", device="cpu", policy_kwargs=dict(features_extractor_class=FeatureExtractor, activation_fn=th.nn.LeakyReLU,ortho_init=True ,net_arch=dict(pi=pi_network, vf=value_network)))  # ,policy_kwargs=dict(activation_fn=th.nn.LeakyReLU,net_arch=dict(pi=[32,64], vf=[64,64])))
  
    return ppo


def get_dqn_agent(config, verbose=1):
    print("init dqn agent")
    print(config)
    dqn = DQN("MlpPolicy", vec_env_dis, verbose=verbose, seed=agent_seed,
              tensorboard_log="./dqn_dflp_discrete_tensorboard/",)
    learning_rate = config["learning_rate"] if "learning_rate" in config else dqn.learning_rate
    batch_size = config["batch_size"]if "batch_size" in config else dqn.batch_size
    learning_starts = config["learning_starts"]if "learning_starts" in config else dqn.learning_starts
    exploration_initial_eps = config["exploration_initial_eps"]if "exploration_initial_eps" in config else dqn.exploration_initial_eps
    exploration_final_eps = config["exploration_final_eps"]if "exploration_final_eps" in config else dqn.exploration_final_eps
    exploration_fraction = config["exploration_fraction"]if "exploration_fraction" in config else dqn.exploration_fraction
    buffer_size = config["buffer_size"]if "buffer_size" in config else dqn.buffer_size
    target_update_interval = config["target_update_interval"]if "target_update_interval" in config else dqn.target_update_interval
    train_freq = config["train_freq"]if "train_freq" in config else dqn.train_freq
    gradient_steps = config["gradient_steps"]if "gradient_steps" in config else dqn.gradient_steps
    tau = config["tau"]if "tau" in config else dqn.tau
    gamma = config["gamma"]if "gamma" in config else dqn.gamma
    network = config["network"]if "network" in config else [64, 64]
    dqn = DQN("MlpPolicy", vec_env_dis, verbose=verbose, seed=agent_seed, learning_rate=float(learning_rate), batch_size=batch_size, buffer_size=buffer_size,
              learning_starts=learning_starts, exploration_initial_eps=exploration_initial_eps, exploration_final_eps=exploration_final_eps,exploration_fraction=exploration_fraction, tau=tau, gamma=gamma,  # device="cpu",
              target_update_interval=target_update_interval, train_freq=train_freq,gradient_steps=gradient_steps, tensorboard_log="./dqn_dflp_discrete_tensorboard/", policy_kwargs=dict(features_extractor_class=FeatureExtractor, net_arch=network))

    return dqn


def get_sac_agent(config, verbose=1):
    print("init sac agent")
    print(config)
    sac = SAC("MlpPolicy", vec_env_cont, verbose=verbose, seed=agent_seed,
              tensorboard_log="./sac_dflp_discrete_tensorboard/")
    learning_rate = config["learning_rate"] if "learning_rate" in config else sac.learning_rate
    batch_size = config["batch_size"] if "batch_size" in config else sac.batch_size
    gradient_steps = config["gradient_steps"]if "gradient_steps" in config else sac.gradient_steps
    buffer_size = config["buffer_size"]if "buffer_size" in config else sac.buffer_size
    train_freq = config["train_freq"]if "train_freq" in config else sac.train_freq
    ent_coef = config["ent_coef"]if "ent_coef" in config else sac.ent_coef
    learning_starts = config["learning_starts"]if "learning_starts" in config else sac.learning_starts
    gamma = config["gamma"]if "gamma" in config else sac.gamma
    tau = config["tau"]if "tau" in config else sac.tau
    pi_network = config["pi_network"]if "pi_network" in config else [64, 64]
    value_network = config["value_network"]if "value_network" in config else [
        64, 64]
   
    sac = SAC("MlpPolicy", vec_env_cont, verbose=verbose, seed=agent_seed, learning_rate=learning_rate, batch_size=batch_size,ent_coef=ent_coef, learning_starts=learning_starts,
              gradient_steps=gradient_steps, buffer_size=buffer_size, train_freq=train_freq, gamma=gamma, tau=tau,
              tensorboard_log="./sac_dflp_discrete_tensorboard/", device="cpu", policy_kwargs=dict(features_extractor_class=FeatureExtractor, activation_fn = th.nn.LeakyReLU,use_sde=False,net_arch=dict(pi=pi_network, qf=value_network)))
    
    return sac



def get_a2c_agent(config, verbose=1):
    print("init a2c agent")
    print(config)
    a2c = A2C("MlpPolicy", vec_env_mul_dis, verbose=verbose, seed=agent_seed,
              tensorboard_log="./a2c_dflp_discrete_tensorboard/")
    learning_rate = linear_schedule(config["learning_rate"]) if "learning_rate" in config else a2c.learning_rate
    n_steps = config["n_steps"]if "n_steps" in config else a2c.n_steps
    ent_coef = config["ent_coef"]if "ent_coef" in config else a2c.ent_coef
    gamma = config["gamma"]if "gamma" in config else a2c.gamma
    gae_lambda = config["gae_lambda"]if "gae_lambda" in config else a2c.gae_lambda
    normalize_advantage= config["normalize_advantage"] if "normalize_advantage" in config else a2c.normalize_advantage
    max_grad_norm = config["max_grad_norm"] if "max_grad_norm" in config else a2c.max_grad_norm
    use_rms_prop= config["use_rms_prop"] if "use_rms_prop" in config else False
    vf_coef = config["vf_coef"] if "vf_coef" in config else a2c.vf_coef
    pi_network = config["pi_network"]if "pi_network" in config else [64, 64]
    value_network = config["value_network"]if "value_network" in config else [64, 64]
    a2c = A2C("MlpPolicy", vec_env_mul_dis, verbose=verbose, seed=agent_seed, learning_rate=learning_rate, n_steps=n_steps,gamma=gamma,max_grad_norm=max_grad_norm,use_rms_prop=use_rms_prop,vf_coef=vf_coef,
              tensorboard_log="./a2c_dflp_discrete_tensorboard/", ent_coef=ent_coef, gae_lambda=gae_lambda, device="cpu", normalize_advantage= normalize_advantage,
               policy_kwargs=dict(features_extractor_class=FeatureExtractor,activation_fn = th.nn.Tanh ,net_arch=dict(pi=pi_network, qf=value_network)))
    return a2c


def runWandb(config,):
    # start a new wandb run to track this script
    config = config

    run = wandb.init(
        # set the wandb project where this run will be logged
        project=f"dflp-{selected_agent}",

        # track hyperparameters and run metadata
        config=config,
        sync_tensorboard=True,  # auto-upload sb3's tensorboard metrics
        monitor_gym=True,  # auto-upload the videos of agents playing the game
        save_code=True,  # optional
    )


def get_agent(agent_name, verbose=1):
    if agent_name == "ppo":
        return get_ppo_agent(config["ppo"], verbose), PPO
    elif agent_name == "dqn":
        return get_dqn_agent(config["dqn"], verbose), DQN
    elif agent_name == "sac":
        return get_sac_agent(config["sac"], verbose), SAC
    elif agent_name == "a2c":
        return get_a2c_agent(config["a2c"], verbose), A2C
    raise Exception("Sorry, no agent matched")


def train(save, agent_name, env, total_timesteps, verbose=1):
    agent, agent_class = get_agent(agent_name=agent_name, verbose=verbose)

    logger.info(config[agent_name])
    callback = SaveOnBestTrainingRewardCallback(
        check_freq=1000, log_dir=log_dir)
    # SaveOnBestTrainingRewardCallback(check_freq=1000, log_dir=log_dir))#, progress_bar=True)
    eval_callback = EvalCallback(env, best_model_save_path=log_dir, n_eval_episodes=1,
                                 log_path=log_dir, eval_freq=1025,
                                 deterministic=True, render=False)
    agent.learn(total_timesteps=total_timesteps,
                tb_log_name=f"{agent_name}_dflp_discrete_tensorboard", callback=eval_callback)
    del agent

    agent = agent_class.load(log_dir+"//best_model.zip", env=env)

    # agent = agent.load()
    if save:
        agent.save(selected_agent)
    mean_reward, std_reward = evaluate_policy(
        agent, agent.get_env(), deterministic=True, render=True, n_eval_episodes=10)
    print(f"mean_reward:{mean_reward:.2f} +/- {std_reward:.2f}")
    return agent


def test(number_of_test_iterations, model, env):

    env.reset()
    sum_rewards = 0
    mean_result = 0
    for _ in range(number_of_test_iterations):

        terminal = False
        state, _ = env.reset()

        sum_rewards = 0
        print("**************************new test***********************")
        while not terminal:
            action, state = model.predict(state, deterministic=True)

            state, reward, terminal, _, _ = env.step(action)
          
            print("action: ", action)
            print("reward: ", reward)
            logger.info(f"action: {action}")
            logger.info(f"reward: {reward}")
            print(env.grid_of_current_time_step)

            sum_rewards += reward
        print("end reward for following: ", sum_rewards)
        logger.info(f"end reward for following: {sum_rewards}")
        logger.info(env.grid)
        print(env.grid)
        mean_result += sum_rewards

    mean_result /= number_of_test_iterations
    return mean_result


def moving_average(values, window):
    """
    Smooth values by doing a moving average
    :param values: (numpy array)
    :param window: (int)
    :return: (numpy array)
    """
    weights = np.repeat(1.0, window) / window
    return np.convolve(values, weights, "valid")


def plot_results(log_folder, title="Learning Curve"):
    """
    plot the results

    :param log_folder: (str) the save location of the results to plot
    :param title: (str) the title of the task to plot
    """
    x, y = ts2xy(load_results(log_folder), "timesteps")
    y = moving_average(y, window=50)
    # Truncate x
    x = x[len(x) - len(y):]

    fig = plt.figure(title)
    plt.plot(x, y)
    plt.xlabel("Number of Timesteps")
    plt.ylabel("Rewards")
    plt.title(title + " Smoothed")
    plt.show()


def hyperparameter_search(agent_name, parametrization, budget, parallel_experiments, num_tests, envi
                          ):
    """
    
    Search for hyperparameters by launching parallel experiments.

    Args:
        game_name (str): Name of the game module, it should match the name of a .py file
        in the "./games" directory.

        parametrization : Nevergrad parametrization, please refer to nevergrad documentation.

        budget (int): Number of experiments to launch in total.

        parallel_experiments (int): Number of experiments to launch in parallel.

        num_tests (int): Number of games to average for evaluating an experiment.

    This code was taken from Muzero and modified  for Stablebaselines
    """

    optimizer = nevergrad.optimizers.OnePlusOne(
        parametrization=parametrization, budget=budget
    )

    running_experiments = []
    best_training = None
    env = envi
    if selected_agent == "sac":
        env = env_cont
    try: 
    # Launch initial experiments
        for i in range(parallel_experiments):
            if 0 < budget:
                param = optimizer.ask()
                print(f"Launching new experiment: {param.value}")
                to_test_config = deepcopy(config[agent_name])
                for key in param.value:
                    config[agent_name][key] = param.value[key]
                trainedAgent = train(env=env, save=False, agent_name=agent_name,
                                    total_timesteps=config[agent_name]["training_steps"], verbose=0)
                running_experiments.append(trainedAgent)
                budget -= 1

        while 0 < budget or any(running_experiments):
            for i, experiment in enumerate(running_experiments):

                result = test(num_tests, trainedAgent, env)
                if not best_training or best_training["result"] < result:
                    best_training = {
                        "result": result,
                        "config": to_test_config,
                        # "checkpoint": experiment.checkpoint,
                    }
                print(f"Parameters: {param.value}")
                print(f"Result: {result}")
                optimizer.tell(param, -result)

                if 0 < budget:
                    param = optimizer.ask()
                    print(f"Launching new experiment: {param.value}")
                    to_test_config = deepcopy(config[agent_name])
                    for key in param.value:
                        config[agent_name][key] = param.value[key]

                    if (should_wandb):
                        runWandb(config=config[agent_name])
                    trainedAgent = train(env=env, save=False, agent_name=agent_name,
                                        total_timesteps=config[agent_name]["training_steps"], verbose=0)
                    print(config)
                    running_experiments[i] = trainedAgent
                    budget -= 1
                else:
                    running_experiments[i] = None
    except:
        agent,agent_class =get_agent(agent_name)
        agent = agent_class.load(log_dir+"//best_model.zip", env=env)
        test(num_tests, agent, env)


    recommendation = optimizer.provide_recommendation()
    print("Best hyperparameters:")
    print(recommendation.value)
    if best_training:
       
        text_file = open(
            "best_parameters_1.txt",
            "w",
        )
        text_file.write(str(recommendation.value))
        text_file.close()
    return recommendation.value


def get_dqn_parametrization():

    learning_rate = nevergrad.p.Log(lower=0.00007, upper=0.001)
    batch_size = nevergrad.p.Choice([64, 128, 256,512, 1024])
    network = nevergrad.p.Choice(
        [[64, 64], [128, 128],[64, 128, 64], [64, 128, 256, 128,64],[256,256]])
    gamma = nevergrad.p.Log(lower=0.90, upper=1.0)
    buffer_size= nevergrad.p.Choice([10000])
    target_update_interval = nevergrad.p.Choice([ 500])
    learning_starts= 1000
    #train_freq = nevergrad.p.Choice([(3, "episode"),(4, "episode"),(5, "episode"),(2,"episode"),(1,"episode")])
    parametrization = nevergrad.p.Dict(#train_freq=train_freq,
        learning_starts=learning_starts, learning_rate=learning_rate, batch_size=batch_size, network=network,gamma=gamma, buffer_size=buffer_size,target_update_interval=target_update_interval)
    
    return parametrization


def get_ppo_parametrization():
    learning_rate = nevergrad.p.Log(lower=0.0001, upper=0.01)
    batch_size = nevergrad.p.Choice([32, 64, 128, 512])
    pi_network = nevergrad.p.Choice(
        [[64, 64], [128, 128], [128, 256, 128], [128, 256, 512, 256, 128]])
    value_network = nevergrad.p.Choice(
        [[64, 64], [128, 128], [128, 256, 128], [128, 256, 512, 256, 128]])
    ent_coef = nevergrad.p.Choice([0.0, 0.01, 0.02,0.03])
    clip_range = nevergrad.p.Choice([0.1, 0.2, 0.3])
    n_steps = nevergrad.p.Choice([16,32,64,128])
    gamma = nevergrad.p.Log(lower=0.95, upper=1.0)
    gae_lambda = nevergrad.p.Log(lower=0.9, upper=1.0)
    n_epochs= nevergrad.p.Choice([ 5,7, 10,15,20])

    parametrization = nevergrad.p.Dict(learning_rate=learning_rate, batch_size=batch_size, pi_network=pi_network, value_network=value_network,n_epochs=n_epochs,
                                       gamma=gamma, clip_range=clip_range, gae_lambda=gae_lambda, ent_coef=ent_coef, n_steps=n_steps)
    return parametrization


def get_sac_parametrization():
    learning_rate = nevergrad.p.Log(lower=0.0001, upper=0.01)
    batch_size = nevergrad.p.Choice([32, 64, 128, 512])
    pi_network = nevergrad.p.Choice(
        [[64, 64], [128, 128], [128, 256, 128], [128, 256, 512, 256, 128]])
    value_network = nevergrad.p.Choice(
        [[64, 64], [128, 128], [128, 256, 128], [128, 256, 512, 256, 128]])
    gamma = nevergrad.p.Log(lower=0.95, upper=1.0)
    tau = nevergrad.p.Log(lower=0.001, upper=0.01)
    train_freq = nevergrad.p.Choice([1, 4, 8, 16, 32, 64, 128, 256, 512])
    gradient_steps = nevergrad.p.Choice([1, 2, 4])
    buffer_size= nevergrad.p.Choice([500, 1000, 5000, 10000,20000])
    target_update_interval = nevergrad.p.Choice([10, 20  ,50, 100])
    parametrization = nevergrad.p.Dict(learning_rate=learning_rate, batch_size=batch_size, pi_network=pi_network,buffer_size=buffer_size,target_update_interval=target_update_interval
                                       ,value_network=value_network, gamma=gamma, tau=tau, train_freq=train_freq, gradient_steps=gradient_steps)
    return parametrization


def get_a2c_parametrization():
    learning_rate = nevergrad.p.Log(lower=0.0001, upper=0.01)
    n_steps = nevergrad.p.Choice([16,32,64,128])
    ent_coef = nevergrad.p.Choice([0.0, 0.01, 0.02,0.03])
    gae_lambda = nevergrad.p.Log(lower=0.9, upper=1.0)
    gamma =  nevergrad.p.Log(lower=0.95, upper=1.0)
    pi_network = nevergrad.p.Choice(
        [[64, 64], [128, 128], [128, 256, 128], [128, 256, 512, 256, 128]])
    value_network = nevergrad.p.Choice(
        [[64, 64], [128, 128], [128, 256, 128], [128, 256, 512, 256, 128]])
    parametrization = nevergrad.p.Dict(learning_rate=learning_rate, n_steps=n_steps, pi_network=pi_network,
                                       value_network=value_network, gamma=gamma, ent_coef=ent_coef, gae_lambda=gae_lambda)
    return parametrization


def get_parametrization(agent_name):
    if agent_name == "dqn":
        return get_dqn_parametrization()
    elif agent_name == "ppo":
        return get_ppo_parametrization()
    elif agent_name == "sac":
        return get_sac_parametrization()
    elif agent_name == "a2c":
        return get_a2c_parametrization()


def tune_hyperParameters():
    budget = 20
    parallel_experiments = 1
    parametrization = get_parametrization(selected_agent)
    return hyperparameter_search(selected_agent, parametrization, budget, parallel_experiments, 1, env)


if __name__ == "__main__":

    if tune_hyper:
        tune_hyperParameters()
    else:
        env = env_mul_dis
        if (should_wandb):
            runWandb(config=config[selected_agent])
        if selected_agent == "sac":
            env = env_cont
        if selected_agent == "dqn":
            env = env_dis
        model = train(True, selected_agent, env,
                      config[selected_agent]["training_steps"])

        test(10, model, env)
        # visualize the learning
        #results_plotter.plot_results(
        #    [log_dir], 1e5, results_plotter.X_TIMESTEPS, f"{selected_agent}_dflp_discrete")
        #plot_results(log_dir)


# test_optimize("C:\\Users\\innoc\\OneDrive\\Desktop\\Master_thesis\\hyper_para", 'random', "none", "")

