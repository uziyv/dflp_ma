import gymnasium as gym
import numpy as np

from flp_environment.envs import environment_discrete

class ObservationWrapper(gym.ObservationWrapper):
    def __init__(self, env:environment_discrete):
        super().__init__(env)
        shape = env.unwrapped.grid.shape
        shape=(shape[0],shape[1],shape[2])# (shape[1],shape[2],shape[0])
        self.observation_space = gym.spaces.Box(shape=shape, low=0, high=1, dtype=float)
        self.env = env

    def observation(self, obs):
        obs = self.env.unwrapped.grid#np.dstack(self.env.unwrapped.grid)
        obs = (obs+3)/(self.env.unwrapped.max_number_in_unit+3)
        return obs

