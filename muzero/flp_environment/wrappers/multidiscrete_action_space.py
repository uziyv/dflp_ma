from gymnasium import Env, logger, spaces, utils
import gymnasium
import numpy as np 

class MultiDiscreteActionSpaceWrapper(gymnasium.Wrapper):
    def __init__(self, env):
        super().__init__(env)
        self.action_space = spaces.MultiDiscrete([env.unwrapped.num_rows,env.unwrapped.num_cols, env.unwrapped.number_of_rotations])


    def step(self, action):
        
        obs, reward, terminated, truncated, info = self.env.step(action)
       
        return obs, reward, terminated, truncated, info