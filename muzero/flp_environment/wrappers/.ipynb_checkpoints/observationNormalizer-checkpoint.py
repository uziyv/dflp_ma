from gymnasium import Env, logger, spaces, utils
import gymnasium
import numpy as np 
class ObservationNormalizerWrapper(gymnasium.Wrapper):
    def __init__(self, env):
        super().__init__(env)
        self.action_space = spaces.Discrete(env.unwrapped.nS*env.unwrapped.number_of_rotations)


    def step(self, action):
       
        obs, reward, terminated, truncated, info = self.env.step(action)

        obs=(obs+3)/8
       
        return obs, reward, terminated, truncated, info
    
    def reset(self,  seed=None, options=None):
        obs, info=self.env.reset(seed=seed,options=options)
        return (obs+3)/8, info