from copy import deepcopy
from tensorforce import  Agent,Environment
import tensorflow as tf
import numpy as np
import gym
from gym import wrappers
from tensorforce.agents import DoubleDQNAgent,DQNAgent,PPOAgent
from tensorforce.core import  TensorforceConfig
from tensorforce.agents import PPOAgent
from tensorforce.core.networks.network import LayeredNetwork
from tensorforce.execution import Runner
from gym.envs.registration import register
from tensorforce.core.parameters import Exponential
import yaml

from flp_environment.wrappers.discrete_action_space import DiscreteActionSpaceWrapper


LEFT = 0
DOWN = 1
RIGHT = 2
UP = 3
NOTHING = 4

ROTATE90 = 0
ROTATE180 = 1
ROTATE270 = 2
ROTATE360 = 3

EMPTY = 0 
OBSTACLE = -1 
FLOW = -2
START_OF_FLOW = -3 
END_OF_FLOW = -4 

desc = [[[EMPTY, EMPTY, EMPTY, START_OF_FLOW,EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY,EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY,EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, END_OF_FLOW, EMPTY,OBSTACLE, EMPTY, OBSTACLE]]]
''',

        [[OBSTACLE, OBSTACLE, EMPTY, START_OF_FLOW,EMPTY, EMPTY, OBSTACLE],
        [EMPTY, EMPTY, EMPTY, EMPTY,EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY,EMPTY, OBSTACLE, EMPTY],
        [EMPTY, EMPTY, END_OF_FLOW, EMPTY,EMPTY, EMPTY, OBSTACLE]],

        [[EMPTY, EMPTY, EMPTY, START_OF_FLOW,EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY,EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, EMPTY, EMPTY,EMPTY, EMPTY, EMPTY],
        [EMPTY, EMPTY, END_OF_FLOW, EMPTY,EMPTY, EMPTY, EMPTY]]
                ]'''
with open('config.yaml', 'r') as file:
    config = yaml.safe_load(file)
with open('env_config.yaml', 'r') as file:
    env_config = yaml.safe_load(file)
use_case = "Use-case-2"
env_config = env_config[use_case]
agent_seed = config["general"]["agent_seed"]
training_steps= config[use_case]["training_steps"]
vpg_config=config[use_case]


def prepare_units(units):
    units_copy = deepcopy(units)
    for i in range(0, len(units_copy)):
        unit = units_copy[i]
        for j in range(0, len(unit)):
            for k in range(0, len(unit[0])):
                unit[j][k] = unit[j][k]+i*3 if unit[j][k] != 0 else 0

    return units_copy


def make_env():
    grids = env_config["grids"]
    units = prepare_units(env_config["units"])
    material_flow = env_config["material_flow"]
    fix_cost = env_config["fix_cost"]
    period = env_config["period"]
    interarrival_time = env_config["interarrival_time"]
    initial_positions = env_config["initial_positions"]
    env = gym.make('flp_discrete-v1', desc=grids, units=units, material_flow=material_flow, fix_cost=fix_cost,
                   interarrival_time=interarrival_time, period_length=period, initial_positions=initial_positions)  # env = ObservationNormalizerWrapper(env)
    # env = NormalizeReward(env)
    # env = TimeLimit(env=env, max_episode_steps=len(units)*len(grids))
    env = DiscreteActionSpaceWrapper(env)
    env = Environment.create(environment=env,max_episode_timesteps=16)
    
    return env



#env= gym.make('flp_discrete-v1', desc= desc, units = units)


#env = wrappers.Monitor(env, 'tmp', force=True)



# Pre-defined or custom environment
environment = make_env() #='gym', level='flp-v2')
#
  

if __name__=="__main__":

    vpg_agent= Agent.create(
    agent='vpg', 
     environment=environment,  
        # Automatically configured network
        network=dict(type='auto', size=vpg_config["size"], depth=vpg_config["depth"]),
        # PPO optimization parameters
        batch_size=vpg_config["batch_size"], update_frequency=vpg_config["update_frequeny"], learning_rate= vpg_config["learning_rate"],

   # ,  discount=0.99, predict_terminal_values=False,
        reward_processing=None,
        # Baseline network and optimizer
      
        # Regularization
        l2_regularization=vpg_config["l2_regularization"], entropy_regularization=vpg_config["entropy_regularization"],
        # Preprocessing
        # Exploration
        exploration=0.0, variable_noise=0.0,
        # Default additional config values
        config=dict(seed=agent_seed),
        # Save agent every 10 updates and keep the 5 most recent checkpoints
        #saver=None#dict(directory='model', frequency=10, max_checkpoints=5),
        # Log all available Tensorboard summaries
        summarizer=dict(directory='summaries/run10',summaries=["reward", "return", "loss"]), #summaries=["reward"]),
        # Do not record agent-environment interaction trace
        recorder=None,

   
)
    print(vpg_agent.config)

    
    agent = vpg_agent



    runner = Runner(agent=agent, environment=environment)

    runner.run(num_episodes=training_steps)#num_timesteps=200000)
    runner.close()
    

    



    print("=========", "start evaluating","============")
    # Evaluate for 100 episodes
    sum_rewards = 0.0
    for _ in range(10):
        sum_rewards = 0.0
        states = environment.reset()
        internals = agent.initial_internals()
        terminal = False
        while not terminal:
            #print(states)
            actions, internals = agent.act(
                states=states, internals=internals, independent=True, deterministic=False
                )
            states, terminal, reward = environment.execute(actions=actions)
            
            sum_rewards += reward
            

        print(environment.environment.grid)
        print(sum_rewards)
    print('Mean evaluation return:', sum_rewards / 100.0)


    agent.close()
    environment.close()
    